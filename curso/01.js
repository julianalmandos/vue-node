const app = new Vue({
    el: '#app',
    data: {
        titulo: 'Hola Mundo',
        frutas: [
            {nombre:'Pera',cantidad:10},
            {nombre:'Manzana', cantidad:20},
            {nombre:'Banana', cantidad:5}
        ],
        nuevaFruta: ''
    },
    methods: {
        agregarFruta() {
            this.frutas.push({
                nombre: this.nuevaFruta,
                cantidad:0
            });
            this.nuevaFruta = ''
        }
    }

});