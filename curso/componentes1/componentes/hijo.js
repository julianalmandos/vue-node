Vue.component('hijo',{
    template: //html
    `
    <div class="py-5 bg-success">
        <h4>Componente hijo, numero {{numero}}</h4>
        <h6>Nombre: {{nombre}}</h6>
        <button @click="incrementarNumero">+</button>
    </div>
    `,
    props: ['numero'],
    data() {
        return {
            nombre: 'Asadasds',
        }
    },
    mounted () {
        this.$emit('nombreHijo',this.nombre);
    },
    methods: {
        incrementarNumero() {
            this.$emit('increment');
        }
    }
})