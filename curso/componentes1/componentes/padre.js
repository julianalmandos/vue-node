Vue.component('padre',{
    template: //html
    `
    <div class="p-5 bg-dark text-white">
        <h2>Componente padre {{contador}}</h2>
        <button class="btn btn-danger" @click="contador++">+</button>
        {{nombreHijo}}
        <input v-model="contador"/>
        <hijo :numero="contador" @nombreHijo="nombreHijo = $event" @increment="contador++"></hijo>
    </div>    
    `,
    data() {
        return {
            contador: 0,
            nombreHijo: ''
        }
    }
})