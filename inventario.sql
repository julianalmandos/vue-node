/*
Navicat MySQL Data Transfer

Source Server         : Symfony
Source Server Version : 50721
Source Host           : localhost:3306
Source Database       : inventario

Target Server Type    : MYSQL
Target Server Version : 50721
File Encoding         : 65001

Date: 2019-04-24 14:17:06
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for departamentos
-- ----------------------------
DROP TABLE IF EXISTS `departamentos`;
CREATE TABLE `departamentos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of departamentos
-- ----------------------------
INSERT INTO `departamentos` VALUES ('1', 'APR');
INSERT INTO `departamentos` VALUES ('2', 'Juzgado');
INSERT INTO `departamentos` VALUES ('3', 'COM');

-- ----------------------------
-- Table structure for imagenes
-- ----------------------------
DROP TABLE IF EXISTS `imagenes`;
CREATE TABLE `imagenes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of imagenes
-- ----------------------------
INSERT INTO `imagenes` VALUES ('1', '1105', 'escudo-mlp.png');
INSERT INTO `imagenes` VALUES ('2', '1106', 'logo.png');
INSERT INTO `imagenes` VALUES ('4', '1107', 'logo-corazon-color-400x121px.png');

-- ----------------------------
-- Table structure for llaves
-- ----------------------------
DROP TABLE IF EXISTS `llaves`;
CREATE TABLE `llaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `user` varchar(255) DEFAULT NULL,
  `fecha_ret` datetime DEFAULT NULL,
  `fecha_dev` datetime DEFAULT NULL,
  `disponible` int(11) NOT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9056CF2E5A91C08D` (`departamento_id`),
  CONSTRAINT `FK_9056CF2E5A91C08D` FOREIGN KEY (`departamento_id`) REFERENCES `departamentos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of llaves
-- ----------------------------
INSERT INTO `llaves` VALUES ('1', 'Llave Roja', 'Armani, Franco', '2018-10-08 14:33:46', '2018-10-08 14:33:51', '1', '1');
INSERT INTO `llaves` VALUES ('2', 'Llave Verde', 'Martínez, Gonzalo', '2018-10-08 14:34:11', '2018-10-08 14:34:37', '1', '1');
INSERT INTO `llaves` VALUES ('3', 'Llave Azul', 'Lanzini, Manuel', '2018-07-18 14:35:34', '2018-07-18 14:35:40', '1', '1');
INSERT INTO `llaves` VALUES ('4', 'Llave Violeta', '', '2018-01-16 16:07:26', '2018-01-18 14:38:29', '1', '1');
INSERT INTO `llaves` VALUES ('5', 'Llave Secretaria', 'Jose Prueba', '2018-02-07 13:03:08', '2018-02-07 13:03:48', '1', '1');
INSERT INTO `llaves` VALUES ('6', 'Llave Producción', 'Jose Prueba 2', '2018-02-07 13:04:03', '2018-02-23 11:59:41', '1', '1');
INSERT INTO `llaves` VALUES ('7', 'Llave Desarrollo', 'asdasd', '2018-01-30 13:05:17', '2018-03-07 13:14:48', '1', '1');
INSERT INTO `llaves` VALUES ('8', 'Llave Servidores', 'aggsd', '2018-02-07 11:41:19', '2018-02-07 11:41:48', '1', '1');
INSERT INTO `llaves` VALUES ('9', 'Llave Principal', 'Rojas, Ariel', '2018-02-01 10:57:25', '2018-02-01 11:20:24', '1', '1');
INSERT INTO `llaves` VALUES ('10', 'Llave Oficina 1', 'Almandos, Julian Luis', '2018-01-26 13:37:21', '2018-01-26 13:44:30', '1', '1');
INSERT INTO `llaves` VALUES ('11', 'Llave Oficina 2', '', '2018-01-18 14:41:36', '2018-01-18 14:55:42', '1', '1');
INSERT INTO `llaves` VALUES ('16', 'Llave Producción', 'Martinez, Lautaro', '2018-03-06 12:28:10', '2018-03-06 12:28:34', '1', '2');
INSERT INTO `llaves` VALUES ('18', 'Llave Pruebaaaa', 'Almandos, Julian Luis', '2018-02-07 13:20:13', '2018-02-07 13:20:42', '1', '2');

-- ----------------------------
-- Table structure for operaciones
-- ----------------------------
DROP TABLE IF EXISTS `operaciones`;
CREATE TABLE `operaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `prod_id` int(11) DEFAULT NULL,
  `op_id` int(11) DEFAULT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `motivo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `area_receptora` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `persona_responsable` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_616A950EA76ED395` (`user_id`),
  KEY `IDX_616A950E1C83F75` (`prod_id`),
  KEY `IDX_616A950E2F7FAB3F` (`op_id`),
  KEY `IDX_616A950E814AA86C` (`dep_id`),
  CONSTRAINT `FK_616A950E1C83F75` FOREIGN KEY (`prod_id`) REFERENCES `productos` (`id`),
  CONSTRAINT `FK_616A950E2F7FAB3F` FOREIGN KEY (`op_id`) REFERENCES `tipos_operaciones` (`id`),
  CONSTRAINT `FK_616A950E814AA86C` FOREIGN KEY (`dep_id`) REFERENCES `departamentos` (`id`),
  CONSTRAINT `FK_616A950EA76ED395` FOREIGN KEY (`user_id`) REFERENCES `usuarios` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=313 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of operaciones
-- ----------------------------
INSERT INTO `operaciones` VALUES ('1', '4', '100', '1', '2', '2018-02-20 13:15:44', null, null, null);
INSERT INTO `operaciones` VALUES ('2', '4', '101', '1', '2', '2018-02-20 13:15:44', null, null, null);
INSERT INTO `operaciones` VALUES ('3', '4', '102', '1', '2', '2018-02-20 13:15:45', null, null, null);
INSERT INTO `operaciones` VALUES ('4', '4', '103', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('5', '4', '104', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('6', '4', '105', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('7', '4', '106', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('8', '4', '107', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('9', '4', '108', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('10', '4', '109', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('11', '4', '110', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('12', '4', '111', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('13', '4', '112', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('14', '4', '113', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('15', '4', '114', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('16', '4', '115', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('17', '4', '116', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('18', '4', '117', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('19', '4', '118', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('20', '4', '119', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('21', '4', '120', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('22', '4', '121', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('23', '4', '122', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('24', '4', '123', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('25', '4', '124', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('26', '4', '125', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('27', '4', '126', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('28', '4', '127', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('29', '4', '128', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('30', '4', '129', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('31', '4', '130', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('32', '4', '131', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('33', '4', '132', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('34', '4', '133', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('35', '4', '134', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('36', '4', '135', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('37', '4', '136', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('38', '4', '137', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('39', '4', '138', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('40', '4', '139', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('41', '4', '140', '1', '2', '2018-02-20 13:15:46', null, null, null);
INSERT INTO `operaciones` VALUES ('42', '4', '141', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('43', '4', '142', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('44', '4', '143', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('45', '4', '144', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('46', '4', '145', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('47', '4', '146', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('48', '4', '147', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('49', '4', '148', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('50', '4', '149', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('51', '4', '150', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('52', '4', '151', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('53', '4', '152', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('54', '4', '153', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('55', '4', '154', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('56', '4', '155', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('57', '4', '156', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('58', '4', '157', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('59', '4', '158', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('60', '4', '159', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('61', '4', '160', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('62', '4', '161', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('63', '4', '162', '1', '2', '2018-02-20 13:15:47', null, null, null);
INSERT INTO `operaciones` VALUES ('64', '4', '163', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('65', '4', '164', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('66', '4', '165', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('67', '4', '166', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('68', '4', '167', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('69', '4', '168', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('70', '4', '169', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('71', '4', '170', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('72', '4', '171', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('73', '4', '172', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('74', '4', '173', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('75', '4', '174', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('76', '4', '175', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('77', '4', '176', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('78', '4', '177', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('79', '4', '178', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('80', '4', '179', '1', '2', '2018-02-20 13:15:48', null, null, null);
INSERT INTO `operaciones` VALUES ('81', '4', '180', '1', '2', '2018-02-20 13:15:49', null, null, null);
INSERT INTO `operaciones` VALUES ('82', '4', '181', '1', '2', '2018-02-20 13:15:49', null, null, null);
INSERT INTO `operaciones` VALUES ('83', '4', '182', '1', '2', '2018-02-20 13:15:49', null, null, null);
INSERT INTO `operaciones` VALUES ('84', '4', '183', '1', '2', '2018-02-20 13:15:49', null, null, null);
INSERT INTO `operaciones` VALUES ('85', '4', '184', '1', '2', '2018-02-20 13:15:49', null, null, null);
INSERT INTO `operaciones` VALUES ('86', '4', '101', '2', '2', '2018-02-20 13:18:28', 'Se entrega a \'Archivo General\' como sustituto provisorio de uno que trajeron p reparar .\r\n', null, null);
INSERT INTO `operaciones` VALUES ('87', '4', '112', '2', '2', '2018-02-20 13:21:25', 'Lo vinieron a retirar de la dirección de empleo.', null, null);
INSERT INTO `operaciones` VALUES ('88', '4', '128', '2', '2', '2018-02-20 13:21:49', 'Destino: COM . Ticket#20170802101515  .  Lo solicita Rimoldi, para Jorge Estévez', null, null);
INSERT INTO `operaciones` VALUES ('89', '4', '129', '2', '2', '2018-02-20 13:21:49', 'Destino: COM . Ticket#20170802101515  .  Lo solicita Rimoldi,para Jorge Estévez', null, null);
INSERT INTO `operaciones` VALUES ('90', '4', '130', '2', '2', '2018-02-20 13:21:49', 'Destino: COM . Ticket#20170802101515  .  Lo solicita Rimoldi, para Jorge Estévez', null, null);
INSERT INTO `operaciones` VALUES ('91', '4', '135', '2', '2', '2018-02-20 13:22:05', 'Destino: Departamento de Distribución, APR. Ticket#201711071013114', null, null);
INSERT INTO `operaciones` VALUES ('92', '4', '136', '2', '2', '2018-02-20 13:22:19', 'Destino: PC del depósito de APR.', null, null);
INSERT INTO `operaciones` VALUES ('93', '4', '137', '2', '2', '2018-02-20 13:22:38', 'Destino: La pide Angel Caro para un  muchacho de una radio. Sin Ticket.', null, null);
INSERT INTO `operaciones` VALUES ('94', '4', '138', '2', '2', '2018-02-20 13:22:51', 'Destino: Cristian Cóceres. ASESORAMIENTO TRIBUTARIO Y CATASTRAL Ticket#201710101012582', null, null);
INSERT INTO `operaciones` VALUES ('95', '4', '140', '2', '2', '2018-02-20 13:23:11', 'Destino: Mongan.     Ticket#201801151014378 — Pedido de PC', null, null);
INSERT INTO `operaciones` VALUES ('96', '4', '142', '2', '2', '2018-02-20 13:23:25', 'Destino: Consejo Deliberante.  Liliana Lucha.  Ticket#201801241014610', null, null);
INSERT INTO `operaciones` VALUES ('97', '4', '147', '2', '2', '2018-02-20 13:23:39', 'Destino: Consejo Deliberante. Liliana Lucha.  Ticket#201801241014610', null, null);
INSERT INTO `operaciones` VALUES ('98', '4', '184', '2', '2', '2018-02-20 13:23:56', 'Delegación de City Bell solicita reemplazo de Monitor TRC Syncmaster. T° 201801181014475', null, null);
INSERT INTO `operaciones` VALUES ('99', '4', '126', '2', '2', '2018-03-14 11:01:37', 'Se  retira para hacer las pruebas de funcionamiento y la imagen para clonar las siguientes', null, null);
INSERT INTO `operaciones` VALUES ('100', '4', '131', '2', '2', '2018-03-14 11:40:29', 'Destino: Prensa, calle 53 N° 716 e/9 y 10. Ticket#201803051015435', null, null);
INSERT INTO `operaciones` VALUES ('101', '4', '133', '2', '2', '2018-03-14 11:52:39', 'Destino: Comercio e industria. Lucas Yebrin. T° no hubo un T de salida, fue de palabra.', null, null);
INSERT INTO `operaciones` VALUES ('102', '4', '148', '2', '2', '2018-04-04 11:31:02', 'Destino: Prensa, calle 53 N° 716 e/9 y 10. Ticket#201803051015435', null, null);
INSERT INTO `operaciones` VALUES ('103', '4', '1000', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('104', '4', '1001', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('105', '4', '1002', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('106', '4', '1003', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('107', '4', '1004', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('108', '4', '1005', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('109', '4', '1006', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('110', '4', '1007', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('111', '4', '1008', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('112', '4', '1009', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('113', '4', '1010', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('114', '4', '1011', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('115', '4', '1012', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('116', '4', '1013', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('117', '4', '1014', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('118', '4', '1015', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('119', '4', '1016', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('120', '4', '1017', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('121', '4', '1018', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('122', '4', '1019', '1', '1', '2018-04-13 11:35:52', '', null, null);
INSERT INTO `operaciones` VALUES ('123', '4', '1020', '1', '1', '2018-04-13 11:35:53', '', null, null);
INSERT INTO `operaciones` VALUES ('124', '4', '1021', '1', '1', '2018-04-13 11:35:53', '', null, null);
INSERT INTO `operaciones` VALUES ('125', '4', '1022', '1', '1', '2018-04-13 11:39:21', '', null, null);
INSERT INTO `operaciones` VALUES ('126', '4', '1023', '1', '1', '2018-04-13 11:39:21', '', null, null);
INSERT INTO `operaciones` VALUES ('127', '4', '1024', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('128', '4', '1025', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('129', '4', '1026', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('130', '4', '1027', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('131', '4', '1028', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('132', '4', '1029', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('133', '4', '1030', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('134', '4', '1031', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('135', '4', '1032', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('136', '4', '1033', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('137', '4', '1034', '1', '1', '2018-04-13 13:04:53', '', null, null);
INSERT INTO `operaciones` VALUES ('138', '4', '1035', '1', '1', '2018-04-13 13:04:54', '', null, null);
INSERT INTO `operaciones` VALUES ('139', '4', '1036', '1', '1', '2018-04-13 13:04:54', '', null, null);
INSERT INTO `operaciones` VALUES ('140', '4', '1037', '1', '1', '2018-04-13 13:04:54', '', null, null);
INSERT INTO `operaciones` VALUES ('141', '4', '1038', '1', '1', '2018-04-13 13:04:54', '', null, null);
INSERT INTO `operaciones` VALUES ('142', '4', '1039', '1', '1', '2018-04-13 13:04:54', '', null, null);
INSERT INTO `operaciones` VALUES ('143', '4', '1040', '1', '1', '2018-04-13 13:04:54', '', null, null);
INSERT INTO `operaciones` VALUES ('144', '4', '1041', '1', '1', '2018-04-13 13:18:05', '', null, null);
INSERT INTO `operaciones` VALUES ('145', '4', '1042', '1', '1', '2018-04-13 13:18:05', '', null, null);
INSERT INTO `operaciones` VALUES ('146', '4', '1043', '1', '1', '2018-04-13 13:18:05', '', null, null);
INSERT INTO `operaciones` VALUES ('147', '4', '1044', '1', '1', '2018-04-13 13:18:05', '', null, null);
INSERT INTO `operaciones` VALUES ('148', '4', '1045', '1', '1', '2018-04-13 13:18:05', '', null, null);
INSERT INTO `operaciones` VALUES ('149', '4', '1046', '1', '1', '2018-04-13 13:22:49', '', null, null);
INSERT INTO `operaciones` VALUES ('150', '4', '1047', '1', '1', '2018-04-13 13:22:49', '', null, null);
INSERT INTO `operaciones` VALUES ('151', '4', '1048', '1', '1', '2018-04-13 13:22:49', '', null, null);
INSERT INTO `operaciones` VALUES ('152', '4', '1049', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('153', '4', '1050', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('154', '4', '1051', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('155', '4', '1052', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('156', '4', '1053', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('157', '4', '1054', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('158', '4', '1055', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('159', '4', '1056', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('160', '4', '1057', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('161', '4', '1058', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('162', '4', '1059', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('163', '4', '1060', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('164', '4', '1061', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('165', '4', '1062', '1', '1', '2018-04-13 13:40:43', '', null, null);
INSERT INTO `operaciones` VALUES ('166', '4', '1063', '1', '1', '2018-04-16 12:15:11', '', null, null);
INSERT INTO `operaciones` VALUES ('167', '4', '1064', '1', '1', '2018-04-16 12:15:11', '', null, null);
INSERT INTO `operaciones` VALUES ('168', '4', '1065', '1', '1', '2018-04-16 12:15:11', '', null, null);
INSERT INTO `operaciones` VALUES ('169', '4', '1066', '1', '1', '2018-04-16 12:17:32', '', null, null);
INSERT INTO `operaciones` VALUES ('170', '4', '1067', '1', '1', '2018-04-16 12:28:49', '', null, null);
INSERT INTO `operaciones` VALUES ('171', '4', '1068', '1', '1', '2018-04-16 12:28:49', '', null, null);
INSERT INTO `operaciones` VALUES ('172', '4', '1069', '1', '1', '2018-04-16 12:28:49', '', null, null);
INSERT INTO `operaciones` VALUES ('173', '4', '1070', '1', '1', '2018-04-16 12:28:49', '', null, null);
INSERT INTO `operaciones` VALUES ('174', '4', '1071', '1', '1', '2018-04-16 12:28:49', '', null, null);
INSERT INTO `operaciones` VALUES ('175', '4', '1072', '1', '1', '2018-04-16 12:28:49', '', null, null);
INSERT INTO `operaciones` VALUES ('176', '4', '1073', '1', '1', '2018-04-16 12:28:49', '', null, null);
INSERT INTO `operaciones` VALUES ('177', '4', '1074', '1', '1', '2018-04-16 12:28:49', '', null, null);
INSERT INTO `operaciones` VALUES ('178', '4', '1075', '1', '1', '2018-04-16 12:28:50', '', null, null);
INSERT INTO `operaciones` VALUES ('179', '4', '1076', '1', '1', '2018-04-16 12:28:50', '', null, null);
INSERT INTO `operaciones` VALUES ('180', '4', '1077', '1', '1', '2018-04-16 12:28:50', '', null, null);
INSERT INTO `operaciones` VALUES ('181', '4', '1078', '1', '1', '2018-04-16 12:28:50', '', null, null);
INSERT INTO `operaciones` VALUES ('182', '4', '1079', '1', '1', '2018-04-16 12:28:50', '', null, null);
INSERT INTO `operaciones` VALUES ('183', '4', '1080', '1', '1', '2018-04-16 12:28:50', '', null, null);
INSERT INTO `operaciones` VALUES ('184', '4', '144', '2', '2', '2018-04-24 09:40:51', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('185', '4', '145', '2', '2', '2018-04-24 09:40:51', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('186', '4', '146', '2', '2', '2018-04-24 09:40:51', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('187', '4', '149', '2', '2', '2018-04-24 09:40:51', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('188', '4', '150', '2', '2', '2018-04-24 09:40:51', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('189', '4', '127', '2', '2', '2018-04-24 09:53:39', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('190', '4', '132', '2', '2', '2018-04-24 09:53:39', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('191', '4', '134', '2', '2', '2018-04-24 09:53:39', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('192', '4', '139', '2', '2', '2018-04-24 09:53:39', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('193', '4', '141', '2', '2', '2018-04-24 09:53:39', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('194', '4', '143', '2', '2', '2018-04-24 09:53:39', 'Se traslada al depósito de APR 13 y 50', null, null);
INSERT INTO `operaciones` VALUES ('195', '4', '185', '1', '2', '2018-04-24 10:10:23', '', null, null);
INSERT INTO `operaciones` VALUES ('196', '4', '186', '1', '2', '2018-04-24 11:02:23', '', null, null);
INSERT INTO `operaciones` VALUES ('197', '4', '187', '1', '2', '2018-04-24 11:16:32', '', null, null);
INSERT INTO `operaciones` VALUES ('198', '4', '188', '1', '2', '2018-04-24 11:18:47', '', null, null);
INSERT INTO `operaciones` VALUES ('199', '4', '189', '1', '2', '2018-04-24 11:31:27', '', null, null);
INSERT INTO `operaciones` VALUES ('200', '4', '190', '1', '2', '2018-04-24 11:31:27', '', null, null);
INSERT INTO `operaciones` VALUES ('201', '4', '191', '1', '2', '2018-04-24 11:31:27', '', null, null);
INSERT INTO `operaciones` VALUES ('202', '4', '1081', '1', '1', '2018-04-24 12:38:23', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('203', '4', '1082', '1', '1', '2018-04-24 12:38:23', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('204', '4', '1083', '1', '1', '2018-04-24 12:38:23', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('205', '4', '1084', '1', '1', '2018-04-24 12:38:23', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('206', '4', '1085', '1', '1', '2018-04-24 12:38:23', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('207', '4', '1086', '1', '1', '2018-04-24 12:38:23', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('208', '4', '1087', '1', '1', '2018-04-24 12:38:24', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('209', '4', '1088', '1', '1', '2018-04-24 12:38:24', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('210', '4', '1089', '1', '1', '2018-04-24 12:38:24', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('211', '4', '1090', '1', '1', '2018-04-24 12:38:24', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('212', '4', '1091', '1', '1', '2018-04-24 12:38:24', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('213', '4', '1092', '1', '1', '2018-04-24 12:38:24', 'Estaban en el Depósito del Juzgado y se trajeron para acá', null, null);
INSERT INTO `operaciones` VALUES ('214', '4', '1024', '2', '1', '2018-05-02 10:35:16', 'Cambio de teclado para Federico Di Tommaso, se lo lleva Mauro', null, null);
INSERT INTO `operaciones` VALUES ('215', '4', '1025', '2', '1', '2018-05-08 13:02:07', 'Cambio de teclado para Federico di Tommaso', null, null);
INSERT INTO `operaciones` VALUES ('216', '4', '1047', '2', '1', '2018-05-15 10:50:17', 'Se entregaron a Agustin Mordasini', null, null);
INSERT INTO `operaciones` VALUES ('217', '4', '1046', '2', '1', '2018-05-15 10:50:17', 'Se entregaron a Agustin Mordasini', null, null);
INSERT INTO `operaciones` VALUES ('218', '4', '1048', '2', '1', '2018-05-15 10:50:17', 'Se entregaron a Agustin Mordasini', null, null);
INSERT INTO `operaciones` VALUES ('219', '4', '1011', '2', '1', '2018-05-15 10:59:13', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('220', '4', '1017', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('221', '4', '1010', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('222', '4', '1049', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('223', '4', '1061', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('224', '4', '1060', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('225', '4', '1086', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('226', '4', '1083', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('227', '4', '1091', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('228', '4', '1041', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('229', '4', '1042', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('230', '4', '1043', '2', '1', '2018-05-15 10:59:14', 'Se retiraron para entregar a pedido de Angel', null, null);
INSERT INTO `operaciones` VALUES ('231', '4', '1022', '2', '1', '2018-05-17 10:42:07', 'Retiro Mauro para el Call Center', null, null);
INSERT INTO `operaciones` VALUES ('232', '4', '1044', '2', '1', '2018-05-17 10:43:50', 'Retira Mauro para Catastro', null, null);
INSERT INTO `operaciones` VALUES ('233', '4', '1066', '2', '1', '2018-05-28 13:29:32', 'Para PC museo DR - ticket 17143', null, null);
INSERT INTO `operaciones` VALUES ('234', '4', '1027', '2', '1', '2018-06-04 14:22:13', 'cambio de teclado jquintero', null, null);
INSERT INTO `operaciones` VALUES ('235', '4', '1030', '2', '1', '2018-06-06 14:08:13', 'teclado para Fiscalización  ticket 1017450', null, null);
INSERT INTO `operaciones` VALUES ('236', '4', '1023', '2', '1', '2018-06-22 12:13:54', 'Para reemplazo CC El Rincon Ticket 17304', null, null);
INSERT INTO `operaciones` VALUES ('237', '4', '1062', '2', '1', '2018-06-22 12:17:49', 'se entrego teclado', null, null);
INSERT INTO `operaciones` VALUES ('238', '4', '1026', '2', '1', '2018-06-28 09:25:57', 'teclado bangho para Fiscalizacion', null, null);
INSERT INTO `operaciones` VALUES ('239', '4', '1058', '2', '1', '2018-07-02 15:40:05', 'A pedido de el Subsecretario Angel Caro y Director de Tecnología Jorge Vilte, se hace entrega de los dos elementos mencionados seleccionados en el sistema y un mouse Genius (serial X7C93879907924) a Sarlo Sergio.', null, null);
INSERT INTO `operaciones` VALUES ('240', '4', '1008', '2', '1', '2018-07-02 15:40:05', 'A pedido de el Subsecretario Angel Caro y Director de Tecnología Jorge Vilte, se hace entrega de los dos elementos mencionados seleccionados en el sistema y un mouse Genius (serial X7C93879907924) a Sarlo Sergio.', null, null);
INSERT INTO `operaciones` VALUES ('241', '4', '171', '2', '2', '2018-07-02 15:45:57', 'Pedido de Angel Caro, para destinar a Sarlo.', null, null);
INSERT INTO `operaciones` VALUES ('242', '4', '190', '2', '2', '2018-07-02 15:50:15', 'Pedido de Angel Caro.', null, null);
INSERT INTO `operaciones` VALUES ('243', '4', '191', '2', '2', '2018-07-02 15:50:15', 'Pedido de Angel Caro.', null, null);
INSERT INTO `operaciones` VALUES ('244', '4', '1045', '2', '1', '2018-07-03 11:18:22', '', null, null);
INSERT INTO `operaciones` VALUES ('246', '4', '1000', '2', '1', '2018-07-16 14:06:32', 'Para nada', 'Ningun area', 'Yo');
INSERT INTO `operaciones` VALUES ('247', '4', '1001', '2', '1', '2018-07-30 16:03:24', null, null, null);
INSERT INTO `operaciones` VALUES ('248', '4', '1002', '2', '1', '2018-07-30 16:03:24', null, null, null);
INSERT INTO `operaciones` VALUES ('249', '4', '1003', '2', '1', '2018-07-30 16:06:40', null, null, null);
INSERT INTO `operaciones` VALUES ('250', '4', '1028', '2', '1', '2018-07-30 16:06:40', null, null, null);
INSERT INTO `operaciones` VALUES ('251', '4', '1080', '2', '1', '2018-07-30 16:06:40', null, null, null);
INSERT INTO `operaciones` VALUES ('252', '4', '1004', '2', '1', '2018-07-30 16:25:41', null, null, null);
INSERT INTO `operaciones` VALUES ('253', '4', '1029', '2', '1', '2018-07-30 16:25:41', null, null, null);
INSERT INTO `operaciones` VALUES ('254', '4', '1100', '1', '1', '2018-07-31 13:13:20', null, null, null);
INSERT INTO `operaciones` VALUES ('255', '4', '1005', '2', '1', '2018-07-31 13:13:20', null, null, null);
INSERT INTO `operaciones` VALUES ('256', '4', '1006', '2', '1', '2018-07-31 13:13:20', null, null, null);
INSERT INTO `operaciones` VALUES ('257', '4', '1101', '1', '1', '2018-07-31 13:15:39', null, null, null);
INSERT INTO `operaciones` VALUES ('258', '4', '1100', '2', '1', '2018-07-31 13:15:39', null, null, null);
INSERT INTO `operaciones` VALUES ('259', '4', '1102', '1', '1', '2018-07-31 13:17:15', null, null, null);
INSERT INTO `operaciones` VALUES ('260', '4', '1009', '2', '1', '2018-07-31 13:17:15', null, null, null);
INSERT INTO `operaciones` VALUES ('261', '4', '1012', '2', '1', '2018-07-31 13:17:15', null, null, null);
INSERT INTO `operaciones` VALUES ('262', '4', '1102', '2', '1', '2018-07-31 13:18:42', '', '', '');
INSERT INTO `operaciones` VALUES ('263', '4', '1103', '1', '1', '2018-07-31 13:32:23', null, null, null);
INSERT INTO `operaciones` VALUES ('264', '4', '1021', '2', '1', '2018-07-31 13:32:23', null, null, null);
INSERT INTO `operaciones` VALUES ('265', '4', '1031', '2', '1', '2018-07-31 13:32:23', null, null, null);
INSERT INTO `operaciones` VALUES ('266', '4', '1050', '2', '1', '2018-07-31 13:32:23', null, null, null);
INSERT INTO `operaciones` VALUES ('267', '4', '1040', '2', '1', '2018-07-31 13:32:23', null, null, null);
INSERT INTO `operaciones` VALUES ('268', '4', '1063', '2', '1', '2018-07-31 13:32:23', null, null, null);
INSERT INTO `operaciones` VALUES ('269', '4', '1071', '2', '1', '2018-07-31 13:32:23', null, null, null);
INSERT INTO `operaciones` VALUES ('270', '4', '1081', '2', '1', '2018-07-31 13:32:23', null, null, null);
INSERT INTO `operaciones` VALUES ('271', '4', '1103', '2', '1', '2018-07-31 13:43:01', '', '', '');
INSERT INTO `operaciones` VALUES ('272', '4', '1007', '2', '1', '2018-07-31 17:04:26', '', '', '');
INSERT INTO `operaciones` VALUES ('273', '4', '1013', '2', '1', '2018-07-31 17:04:26', '', '', '');
INSERT INTO `operaciones` VALUES ('274', '4', '1014', '2', '1', '2018-07-31 17:06:14', '', '', '');
INSERT INTO `operaciones` VALUES ('275', '4', '1015', '2', '1', '2018-07-31 17:06:14', '', '', '');
INSERT INTO `operaciones` VALUES ('276', '4', '1103', '1', '1', '2018-08-03 14:27:17', 'Volvió al depósito', null, null);
INSERT INTO `operaciones` VALUES ('277', '4', '1103', '2', '1', '2018-08-03 14:32:42', 'Se llevó a la torre', 'Torre 1', 'Lucas Alario');
INSERT INTO `operaciones` VALUES ('278', '4', '192', '1', '2', '2018-08-06 14:37:09', null, null, null);
INSERT INTO `operaciones` VALUES ('279', '4', '100', '2', '2', '2018-08-06 14:37:09', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('280', '4', '102', '2', '2', '2018-08-06 14:37:09', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('281', '4', '193', '1', '2', '2018-08-06 14:39:15', null, null, null);
INSERT INTO `operaciones` VALUES ('282', '4', '180', '2', '2', '2018-08-06 14:39:15', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('283', '4', '181', '2', '2', '2018-08-06 14:39:15', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('284', '4', '194', '1', '2', '2018-08-06 14:41:03', null, null, null);
INSERT INTO `operaciones` VALUES ('285', '4', '103', '2', '2', '2018-08-06 14:41:03', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('286', '4', '104', '2', '2', '2018-08-06 14:41:03', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('287', '4', '195', '1', '2', '2018-08-06 14:44:16', null, null, null);
INSERT INTO `operaciones` VALUES ('288', '4', '105', '2', '2', '2018-08-06 14:44:16', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('289', '4', '106', '2', '2', '2018-08-06 14:44:16', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('290', '4', '107', '2', '2', '2018-08-06 14:55:16', 'No se', 'Pinto', '');
INSERT INTO `operaciones` VALUES ('291', '4', '108', '2', '2', '2018-08-06 14:55:16', 'No se', 'Pinto2', '');
INSERT INTO `operaciones` VALUES ('292', '4', '1021', '1', '1', '2018-08-06 15:16:58', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('293', '4', '1063', '1', '1', '2018-08-06 15:17:18', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('294', '4', '1081', '1', '1', '2018-08-06 15:19:47', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('295', '4', '1040', '1', '1', '2018-08-06 15:19:49', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('296', '4', '1050', '1', '1', '2018-08-06 15:19:51', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('297', '4', '1031', '1', '1', '2018-08-06 15:19:53', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('298', '4', '1071', '1', '1', '2018-08-06 15:19:57', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('299', '4', '196', '1', '2', '2018-08-06 15:24:16', null, null, null);
INSERT INTO `operaciones` VALUES ('300', '4', '197', '1', '2', '2018-08-06 15:26:58', null, null, null);
INSERT INTO `operaciones` VALUES ('301', '4', '198', '1', '2', '2018-08-06 15:27:18', null, null, null);
INSERT INTO `operaciones` VALUES ('302', '4', '199', '1', '2', '2018-08-06 15:27:42', null, null, null);
INSERT INTO `operaciones` VALUES ('303', '4', '109', '2', '2', '2018-08-06 15:27:42', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('304', '4', '110', '2', '2', '2018-08-06 15:27:42', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('305', '4', '111', '2', '2', '2018-08-06 15:27:42', 'Se usó para armar un paquete.', null, null);
INSERT INTO `operaciones` VALUES ('306', '4', '109', '1', '2', '2018-08-06 15:29:33', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('307', '4', '110', '1', '2', '2018-08-06 15:29:36', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('308', '4', '1100', '1', '1', '2018-09-13 13:08:05', 'Se eliminó del paquete al que pertenecía.', null, null);
INSERT INTO `operaciones` VALUES ('309', '4', '1104', '1', '1', '2018-09-14 14:07:17', 'ad', null, null);
INSERT INTO `operaciones` VALUES ('310', '4', '1105', '1', '1', '2018-09-14 14:08:15', 'ad', null, null);
INSERT INTO `operaciones` VALUES ('311', '4', '1106', '1', '1', '2018-09-14 14:19:05', '', null, null);
INSERT INTO `operaciones` VALUES ('312', '4', '1107', '1', '1', '2019-01-22 13:49:25', '', null, null);

-- ----------------------------
-- Table structure for operacionesllaves
-- ----------------------------
DROP TABLE IF EXISTS `operacionesllaves`;
CREATE TABLE `operacionesllaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `llave_id` int(11) DEFAULT NULL,
  `op_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `dep_id` int(11) DEFAULT NULL,
  `responsable` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AE34B04E8EB29E8F` (`llave_id`),
  KEY `IDX_AE34B04E2F7FAB3F` (`op_id`),
  KEY `IDX_AE34B04E814AA86C` (`dep_id`),
  CONSTRAINT `FK_AE34B04E2F7FAB3F` FOREIGN KEY (`op_id`) REFERENCES `tipos_operaciones` (`id`),
  CONSTRAINT `FK_AE34B04E814AA86C` FOREIGN KEY (`dep_id`) REFERENCES `departamentos` (`id`),
  CONSTRAINT `FK_AE34B04E8EB29E8F` FOREIGN KEY (`llave_id`) REFERENCES `llaves` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of operacionesllaves
-- ----------------------------
INSERT INTO `operacionesllaves` VALUES ('1', '4', '9', '2', '2018-02-01 10:57:26', '1', 'Rojas, Ariel');
INSERT INTO `operacionesllaves` VALUES ('2', '4', '5', '2', '2018-02-01 10:57:41', '1', 'Maidana, Jonathan');
INSERT INTO `operacionesllaves` VALUES ('3', '4', '5', '1', '2018-02-01 10:57:49', '1', 'Maidana, Jonathan');
INSERT INTO `operacionesllaves` VALUES ('4', '4', '8', '1', '2018-02-01 11:20:17', '1', 'asdasd');
INSERT INTO `operacionesllaves` VALUES ('5', '4', '9', '1', '2018-02-01 11:20:24', '1', 'Rojas, Ariel');
INSERT INTO `operacionesllaves` VALUES ('6', '4', '8', '2', '2018-02-07 11:41:19', '1', 'aggsd');
INSERT INTO `operacionesllaves` VALUES ('7', '4', '8', '1', '2018-02-07 11:41:48', '1', 'aggsd');
INSERT INTO `operacionesllaves` VALUES ('8', '4', '1', '2', '2018-02-07 12:59:52', '2', 'Almandos, Julian Luis');
INSERT INTO `operacionesllaves` VALUES ('9', '4', '1', '2', '2018-02-07 12:59:52', '2', 'Almandos, Julian Luis');
INSERT INTO `operacionesllaves` VALUES ('10', '4', '1', '1', '2018-02-07 12:59:52', '2', 'Almandos, Julian Luis');
INSERT INTO `operacionesllaves` VALUES ('11', '4', '5', '1', '2018-02-07 13:03:49', '1', 'Jose Prueba');
INSERT INTO `operacionesllaves` VALUES ('12', '4', '6', '2', '2018-02-07 13:04:03', '1', 'Jose Prueba 2');
INSERT INTO `operacionesllaves` VALUES ('13', '4', '16', '2', '2018-02-07 13:13:11', '1', 'Martinez, Lautaro');
INSERT INTO `operacionesllaves` VALUES ('14', '4', '16', '1', '2018-02-07 13:13:30', '1', 'Martinez, Lautaro');
INSERT INTO `operacionesllaves` VALUES ('15', '4', '18', '2', '2018-02-07 13:20:13', '2', 'Almandos, Julian Luis');
INSERT INTO `operacionesllaves` VALUES ('16', '4', '18', '1', '2018-02-07 13:20:42', '2', 'Almandos, Julian Luis');
INSERT INTO `operacionesllaves` VALUES ('17', '4', '1', '2', '2018-02-16 10:56:41', '1', '');
INSERT INTO `operacionesllaves` VALUES ('18', '4', '2', '2', '2018-02-16 11:01:01', '1', '');
INSERT INTO `operacionesllaves` VALUES ('19', '4', '1', '1', '2018-02-16 11:32:32', '1', '');
INSERT INTO `operacionesllaves` VALUES ('20', '4', '2', '1', '2018-02-16 11:32:47', '1', '');
INSERT INTO `operacionesllaves` VALUES ('21', '4', '6', '1', '2018-02-23 11:59:41', '1', 'Jose Prueba 2');
INSERT INTO `operacionesllaves` VALUES ('22', '4', '16', '2', '2018-03-06 12:28:11', '2', 'Martinez, Lautaro');
INSERT INTO `operacionesllaves` VALUES ('23', '4', '16', '1', '2018-03-06 12:28:35', '2', 'Martinez, Lautaro');
INSERT INTO `operacionesllaves` VALUES ('24', '4', '7', '1', '2018-03-07 13:14:48', '1', 'asdasd');
INSERT INTO `operacionesllaves` VALUES ('25', '4', '1', '2', '2018-03-07 13:34:58', '1', 'Almandos, Julian Luis');
INSERT INTO `operacionesllaves` VALUES ('26', '4', '1', '1', '2018-03-07 13:35:05', '1', 'Almandos, Julian Luis');
INSERT INTO `operacionesllaves` VALUES ('27', '4', '1', '1', '2018-07-13 13:06:50', '1', 'Almandos, Julian');
INSERT INTO `operacionesllaves` VALUES ('28', '4', '1', '2', '2018-07-13 13:06:56', '1', 'Almandos, Julian');
INSERT INTO `operacionesllaves` VALUES ('29', '4', '1', '1', '2018-07-16 14:40:17', '1', 'Almandos, Julian');
INSERT INTO `operacionesllaves` VALUES ('30', '4', '3', '2', '2018-07-18 14:35:34', '1', 'Lanzini, Manuel');
INSERT INTO `operacionesllaves` VALUES ('31', '4', '3', '1', '2018-07-18 14:35:40', '1', 'Lanzini, Manuel');
INSERT INTO `operacionesllaves` VALUES ('32', '4', '1', '2', '2018-10-08 14:33:46', '1', 'Armani, Franco');
INSERT INTO `operacionesllaves` VALUES ('33', '4', '1', '1', '2018-10-08 14:33:51', '1', 'Armani, Franco');
INSERT INTO `operacionesllaves` VALUES ('34', '4', '2', '2', '2018-10-08 14:34:11', '1', 'Martínez, Gonzalo');
INSERT INTO `operacionesllaves` VALUES ('35', '4', '2', '1', '2018-10-08 14:34:37', '1', 'Martínez, Gonzalo');

-- ----------------------------
-- Table structure for operacionestoner
-- ----------------------------
DROP TABLE IF EXISTS `operacionestoner`;
CREATE TABLE `operacionestoner` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `toner_id` int(11) NOT NULL,
  `op_id` int(11) DEFAULT NULL,
  `fecha` datetime NOT NULL,
  `cantidad` int(11) NOT NULL,
  `comentario` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_41F58478BDC9E2D` (`toner_id`),
  KEY `IDX_41F584782F7FAB3F` (`op_id`),
  CONSTRAINT `FK_41F584782F7FAB3F` FOREIGN KEY (`op_id`) REFERENCES `tipos_operaciones` (`id`),
  CONSTRAINT `FK_41F58478BDC9E2D` FOREIGN KEY (`toner_id`) REFERENCES `toners` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of operacionestoner
-- ----------------------------
INSERT INTO `operacionestoner` VALUES ('1', '0', '1', '2', '2018-01-31 12:49:32', '1', 'Martínez, Lautaro');
INSERT INTO `operacionestoner` VALUES ('2', '0', '1', '1', '2018-01-31 12:49:41', '24', null);
INSERT INTO `operacionestoner` VALUES ('3', '0', '4', '2', '2018-02-16 11:17:07', '30', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('4', '0', '6', '2', '2018-02-19 11:30:59', '100', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('5', '0', '4', '2', '2018-02-19 11:32:02', '2', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('6', '0', '4', '2', '2018-02-19 11:32:15', '20', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('7', '0', '5', '2', '2018-02-19 11:32:45', '10', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('8', '0', '3', '2', '2018-02-19 11:32:55', '13', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('9', '0', '7', '1', '2018-02-19 11:33:48', '30', null);
INSERT INTO `operacionestoner` VALUES ('10', '0', '1', '2', '2018-03-22 09:32:58', '13', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('11', '0', '6', '2', '2018-03-23 09:43:15', '12', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('12', '0', '2', '2', '2018-03-23 09:43:27', '5', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('13', '0', '2', '2', '2018-03-23 09:43:48', '5', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('14', '0', '1', '1', '2018-03-23 09:44:29', '5', null);
INSERT INTO `operacionestoner` VALUES ('15', '0', '5', '2', '2018-04-09 11:11:06', '5', 'Almandos, Julian Luis');
INSERT INTO `operacionestoner` VALUES ('16', '0', '1', '1', '2018-05-02 09:35:00', '5', null);
INSERT INTO `operacionestoner` VALUES ('17', '0', '1', '1', '2018-05-02 09:35:15', '5', null);
INSERT INTO `operacionestoner` VALUES ('18', '0', '1', '2', '2018-05-02 09:35:41', '10', '');
INSERT INTO `operacionestoner` VALUES ('19', '4', '1', '2', '2018-10-08 14:13:13', '3', 'Ticket 355555');
INSERT INTO `operacionestoner` VALUES ('20', '4', '1', '2', '2018-10-08 14:18:27', '3', 'Ticket Nada');
INSERT INTO `operacionestoner` VALUES ('21', '4', '1', '2', '2018-10-08 14:36:15', '3', 'Tickeeeeeeeeeeet');
INSERT INTO `operacionestoner` VALUES ('22', '4', '8', '2', '2018-10-11 12:45:08', '3', '333333333333333333');
INSERT INTO `operacionestoner` VALUES ('23', '4', '1', '1', '2018-10-29 14:25:28', '5', null);

-- ----------------------------
-- Table structure for paquetes
-- ----------------------------
DROP TABLE IF EXISTS `paquetes`;
CREATE TABLE `paquetes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prod_id` int(11) DEFAULT NULL,
  `paquete_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9B79C0AA41E2D57E` (`paquete_id`),
  KEY `IDX_9B79C0AA1C83F75` (`prod_id`),
  CONSTRAINT `FK_9B79C0AA1C83F75` FOREIGN KEY (`prod_id`) REFERENCES `productos` (`id`),
  CONSTRAINT `FK_9B79C0AA41E2D57E` FOREIGN KEY (`paquete_id`) REFERENCES `productos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paquetes
-- ----------------------------
INSERT INTO `paquetes` VALUES ('1', '1001', '1097');
INSERT INTO `paquetes` VALUES ('2', '1002', '1097');
INSERT INTO `paquetes` VALUES ('3', '1003', '1098');
INSERT INTO `paquetes` VALUES ('4', '1028', '1098');
INSERT INTO `paquetes` VALUES ('5', '1080', '1098');
INSERT INTO `paquetes` VALUES ('6', '1004', '1099');
INSERT INTO `paquetes` VALUES ('7', '1029', '1099');
INSERT INTO `paquetes` VALUES ('8', '1005', '1100');
INSERT INTO `paquetes` VALUES ('9', '1006', '1100');
INSERT INTO `paquetes` VALUES ('11', '1009', '1102');
INSERT INTO `paquetes` VALUES ('12', '1012', '1102');
INSERT INTO `paquetes` VALUES ('20', '100', '192');
INSERT INTO `paquetes` VALUES ('21', '102', '192');
INSERT INTO `paquetes` VALUES ('22', '180', '193');
INSERT INTO `paquetes` VALUES ('23', '181', '193');
INSERT INTO `paquetes` VALUES ('24', '103', '194');
INSERT INTO `paquetes` VALUES ('25', '104', '194');
INSERT INTO `paquetes` VALUES ('26', '105', '195');
INSERT INTO `paquetes` VALUES ('27', '106', '195');
INSERT INTO `paquetes` VALUES ('30', '111', '199');

-- ----------------------------
-- Table structure for partidas_apr
-- ----------------------------
DROP TABLE IF EXISTS `partidas_apr`;
CREATE TABLE `partidas_apr` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `ingresado_por` int(11) NOT NULL,
  `fecha_ingreso` datetime NOT NULL,
  `fecha_vacio` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of partidas_apr
-- ----------------------------
INSERT INTO `partidas_apr` VALUES ('1', '-13', '1', '2017-12-15 15:43:16', '2017-12-15 16:00:49');
INSERT INTO `partidas_apr` VALUES ('2', '0', '1', '2017-12-15 15:43:16', '2017-12-18 13:59:39');
INSERT INTO `partidas_apr` VALUES ('3', '-1', '1', '2017-12-18 13:55:35', null);
INSERT INTO `partidas_apr` VALUES ('4', '1', '1', '2017-12-18 13:56:54', null);
INSERT INTO `partidas_apr` VALUES ('5', '0', '1', '2017-12-18 13:57:49', '2017-12-18 13:59:39');
INSERT INTO `partidas_apr` VALUES ('6', '0', '1', '2017-12-18 14:01:48', '2017-12-18 14:02:00');
INSERT INTO `partidas_apr` VALUES ('7', '0', '1', '2017-12-18 14:03:28', '2017-12-18 14:16:23');
INSERT INTO `partidas_apr` VALUES ('8', '1', '4', '2017-12-18 14:45:10', null);
INSERT INTO `partidas_apr` VALUES ('9', '-1', '4', '2017-12-18 15:10:47', '2017-12-18 15:27:25');
INSERT INTO `partidas_apr` VALUES ('10', '1', '4', '2017-12-19 14:46:04', null);
INSERT INTO `partidas_apr` VALUES ('11', '1', '4', '2017-12-19 14:47:03', null);
INSERT INTO `partidas_apr` VALUES ('12', '1', '4', '2017-12-19 14:47:21', null);
INSERT INTO `partidas_apr` VALUES ('13', '1', '4', '2017-12-19 14:47:51', '2017-12-19 14:48:43');
INSERT INTO `partidas_apr` VALUES ('14', '0', '4', '2017-12-19 15:57:20', '2017-12-19 15:57:29');
INSERT INTO `partidas_apr` VALUES ('15', '0', '4', '2017-12-19 16:29:01', '2017-12-19 17:06:48');
INSERT INTO `partidas_apr` VALUES ('16', '0', '4', '2017-12-19 17:08:29', '2017-12-19 17:09:20');
INSERT INTO `partidas_apr` VALUES ('17', '0', '4', '2017-12-19 17:35:25', '2017-12-20 13:27:41');
INSERT INTO `partidas_apr` VALUES ('18', '0', '4', '2017-12-19 17:35:41', '2017-12-20 13:35:55');
INSERT INTO `partidas_apr` VALUES ('19', '0', '4', '2017-12-19 17:36:25', '2017-12-20 13:58:49');
INSERT INTO `partidas_apr` VALUES ('20', '0', '4', '2017-12-19 17:37:40', '2017-12-20 13:35:42');
INSERT INTO `partidas_apr` VALUES ('21', '0', '4', '2017-12-20 13:55:05', '2017-12-20 13:59:41');
INSERT INTO `partidas_apr` VALUES ('22', '0', '4', '2017-12-20 13:55:21', '2017-12-20 13:58:49');
INSERT INTO `partidas_apr` VALUES ('23', '0', '4', '2017-12-20 14:14:38', '2017-12-21 15:15:11');
INSERT INTO `partidas_apr` VALUES ('24', '0', '4', '2017-12-20 14:14:49', '2017-12-22 16:15:50');
INSERT INTO `partidas_apr` VALUES ('25', '0', '4', '2017-12-20 15:05:54', '2017-12-21 15:15:12');
INSERT INTO `partidas_apr` VALUES ('26', '0', '4', '2017-12-22 15:03:08', '2017-12-22 16:15:50');
INSERT INTO `partidas_apr` VALUES ('27', '0', '4', '2017-12-22 15:03:59', '2017-12-26 15:07:37');
INSERT INTO `partidas_apr` VALUES ('28', '0', '4', '2017-12-22 15:04:50', '2017-12-28 12:35:57');
INSERT INTO `partidas_apr` VALUES ('29', '0', '4', '2017-12-22 15:04:50', '2017-12-26 15:07:38');
INSERT INTO `partidas_apr` VALUES ('30', '0', '4', '2017-12-22 15:04:50', '2017-12-26 13:20:20');
INSERT INTO `partidas_apr` VALUES ('31', '0', '4', '2017-12-22 15:04:50', '2017-12-28 12:35:58');
INSERT INTO `partidas_apr` VALUES ('32', '0', '4', '2017-12-22 15:04:50', '2017-12-22 16:18:27');
INSERT INTO `partidas_apr` VALUES ('33', '0', '4', '2017-12-22 15:04:50', '2017-12-22 16:18:27');
INSERT INTO `partidas_apr` VALUES ('34', '0', '4', '2017-12-22 15:04:50', '2017-12-26 13:20:20');
INSERT INTO `partidas_apr` VALUES ('35', '0', '4', '2017-12-26 16:03:22', '2017-12-29 16:24:39');
INSERT INTO `partidas_apr` VALUES ('36', '0', '4', '2017-12-26 16:07:16', '2017-12-29 16:24:39');
INSERT INTO `partidas_apr` VALUES ('37', '0', '4', '2017-12-26 16:07:35', '2017-12-29 16:24:39');
INSERT INTO `partidas_apr` VALUES ('38', '-2', '4', '2017-12-26 16:07:56', '2017-12-29 16:24:39');
INSERT INTO `partidas_apr` VALUES ('39', '-6', '4', '2017-12-26 16:17:54', '2017-12-28 12:37:15');
INSERT INTO `partidas_apr` VALUES ('40', '-2', '4', '2017-12-27 15:00:11', '2017-12-28 12:37:15');
INSERT INTO `partidas_apr` VALUES ('41', '0', '4', '2017-12-28 12:25:22', '2017-12-28 12:38:26');
INSERT INTO `partidas_apr` VALUES ('42', '0', '4', '2017-12-28 12:34:53', '2017-12-28 12:36:45');
INSERT INTO `partidas_apr` VALUES ('43', '0', '4', '2017-12-28 12:51:59', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('44', '0', '4', '2017-12-28 12:52:43', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('45', '0', '4', '2017-12-28 12:54:20', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('46', '0', '4', '2017-12-28 12:55:04', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('47', '0', '4', '2017-12-28 12:57:05', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('48', '0', '4', '2017-12-28 13:02:09', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('49', '0', '4', '2017-12-28 13:04:34', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('50', '-5', '4', '2017-12-28 13:05:21', '2017-12-29 16:24:37');
INSERT INTO `partidas_apr` VALUES ('51', '0', '4', '2017-12-29 13:25:23', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('52', '0', '4', '2017-12-29 13:44:54', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('53', '0', '4', '2017-12-29 13:46:06', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('54', '0', '4', '2017-12-29 13:47:56', '2017-12-29 16:25:03');
INSERT INTO `partidas_apr` VALUES ('55', '-6', '4', '2017-12-29 17:04:22', '2017-12-29 17:06:55');
INSERT INTO `partidas_apr` VALUES ('56', '0', '4', '2018-01-15 13:19:47', '2018-01-16 15:52:39');
INSERT INTO `partidas_apr` VALUES ('57', '0', '4', '2018-01-15 15:45:16', '2018-01-16 13:27:29');
INSERT INTO `partidas_apr` VALUES ('58', '-2', '4', '2018-01-16 15:41:45', '2018-01-16 15:54:08');
INSERT INTO `partidas_apr` VALUES ('59', '0', '4', '2018-01-16 15:55:15', '2018-01-16 16:51:29');
INSERT INTO `partidas_apr` VALUES ('60', '0', '4', '2018-01-16 16:16:24', '2018-01-16 16:51:29');
INSERT INTO `partidas_apr` VALUES ('61', '-2', '4', '2018-01-16 16:18:55', '2018-01-19 11:27:31');
INSERT INTO `partidas_apr` VALUES ('62', '0', '4', '2018-01-16 16:19:25', '2018-01-23 12:42:30');
INSERT INTO `partidas_apr` VALUES ('63', '0', '4', '2018-01-17 14:38:18', '2018-01-17 15:28:52');
INSERT INTO `partidas_apr` VALUES ('64', '0', '4', '2018-01-17 14:39:38', '2018-01-17 15:29:17');
INSERT INTO `partidas_apr` VALUES ('65', '0', '4', '2018-01-17 14:40:28', null);
INSERT INTO `partidas_apr` VALUES ('66', '0', '4', '2018-01-17 14:41:10', null);
INSERT INTO `partidas_apr` VALUES ('67', '0', '4', '2018-01-17 14:41:52', null);
INSERT INTO `partidas_apr` VALUES ('68', '0', '4', '2018-01-18 13:14:24', '2018-01-18 13:33:15');
INSERT INTO `partidas_apr` VALUES ('69', '0', '4', '2018-01-18 14:07:01', '2018-01-18 14:13:26');
INSERT INTO `partidas_apr` VALUES ('70', '0', '4', '2018-01-18 14:14:29', '2018-01-23 12:42:30');
INSERT INTO `partidas_apr` VALUES ('71', '0', '4', '2018-01-19 10:30:10', '2018-01-23 12:42:30');
INSERT INTO `partidas_apr` VALUES ('72', '-1', '4', '2018-01-22 12:16:12', '2018-01-23 12:42:30');
INSERT INTO `partidas_apr` VALUES ('73', '0', '4', '2018-01-22 12:30:33', '2018-01-23 12:42:30');
INSERT INTO `partidas_apr` VALUES ('74', '-4', '4', '2018-01-22 13:09:16', '2018-01-23 12:42:30');
INSERT INTO `partidas_apr` VALUES ('75', '0', '4', '2018-01-22 13:11:46', '2018-01-23 12:42:30');
INSERT INTO `partidas_apr` VALUES ('76', '-2', '4', '2018-01-22 13:16:01', '2018-01-23 12:42:30');
INSERT INTO `partidas_apr` VALUES ('77', '0', '4', '2018-01-22 13:17:51', '2018-01-23 12:42:30');
INSERT INTO `partidas_apr` VALUES ('78', '-1', '4', '2018-01-23 11:03:48', '2018-01-23 11:55:44');
INSERT INTO `partidas_apr` VALUES ('79', '0', '4', '2018-01-23 12:50:37', '2018-01-26 13:35:03');
INSERT INTO `partidas_apr` VALUES ('80', '1', '4', '2018-01-23 12:55:29', null);
INSERT INTO `partidas_apr` VALUES ('81', '3', '4', '2018-01-24 10:31:35', null);
INSERT INTO `partidas_apr` VALUES ('82', '0', '4', '2018-01-24 11:04:40', '2018-01-29 11:03:18');
INSERT INTO `partidas_apr` VALUES ('83', '2', '4', '2018-01-30 12:59:14', null);
INSERT INTO `partidas_apr` VALUES ('84', '-1', '4', '2018-02-07 12:02:14', '2018-02-07 12:05:15');
INSERT INTO `partidas_apr` VALUES ('85', '-2', '4', '2018-02-08 12:36:00', '2018-02-08 12:40:53');
INSERT INTO `partidas_apr` VALUES ('86', '0', '4', '2018-02-14 10:05:14', '2018-02-14 11:02:40');
INSERT INTO `partidas_apr` VALUES ('87', '0', '4', '2018-02-14 10:07:43', '2018-02-14 10:59:37');
INSERT INTO `partidas_apr` VALUES ('88', '0', '4', '2018-02-15 11:18:24', '2018-02-16 11:25:38');
INSERT INTO `partidas_apr` VALUES ('89', '0', '4', '2018-02-15 14:54:42', null);
INSERT INTO `partidas_apr` VALUES ('90', '0', '4', '2018-02-15 14:56:23', null);
INSERT INTO `partidas_apr` VALUES ('91', '0', '4', '2018-02-15 14:56:45', null);
INSERT INTO `partidas_apr` VALUES ('92', '0', '4', '2018-02-15 14:57:39', '2018-02-16 11:38:07');
INSERT INTO `partidas_apr` VALUES ('93', '0', '4', '2018-02-15 14:57:56', '2018-02-16 11:25:38');
INSERT INTO `partidas_apr` VALUES ('94', '0', '4', '2018-02-15 15:41:51', '2018-02-16 11:38:07');
INSERT INTO `partidas_apr` VALUES ('95', '0', '4', '2018-02-16 10:43:06', '2018-02-16 11:38:07');
INSERT INTO `partidas_apr` VALUES ('96', '-1', '4', '2018-02-16 10:50:25', '2018-02-16 11:25:39');
INSERT INTO `partidas_apr` VALUES ('97', '0', '4', '2018-02-16 10:50:45', '2018-02-16 11:19:29');
INSERT INTO `partidas_apr` VALUES ('98', '0', '4', '2018-02-19 10:33:34', '2018-02-26 12:21:18');
INSERT INTO `partidas_apr` VALUES ('99', '0', '4', '2018-02-20 11:29:34', '2018-02-26 11:43:34');
INSERT INTO `partidas_apr` VALUES ('100', '0', '4', '2018-02-20 11:39:52', '2018-02-23 13:11:32');
INSERT INTO `partidas_apr` VALUES ('101', '0', '4', '2018-02-21 11:08:39', '2018-02-23 13:10:37');
INSERT INTO `partidas_apr` VALUES ('102', '-1', '4', '2018-02-21 11:36:37', '2018-02-23 12:54:08');
INSERT INTO `partidas_apr` VALUES ('103', '1', '4', '2018-02-23 11:39:11', null);
INSERT INTO `partidas_apr` VALUES ('104', '1', '4', '2018-02-26 12:34:49', null);
INSERT INTO `partidas_apr` VALUES ('105', '0', '4', '2018-02-26 13:22:21', null);
INSERT INTO `partidas_apr` VALUES ('106', '0', '4', '2018-02-26 13:26:05', '2018-02-27 11:45:52');
INSERT INTO `partidas_apr` VALUES ('107', '0', '4', '2018-02-26 13:29:33', '2018-02-27 11:25:49');
INSERT INTO `partidas_apr` VALUES ('108', '0', '4', '2018-02-27 11:41:40', '2018-02-27 11:43:06');
INSERT INTO `partidas_apr` VALUES ('109', '0', '4', '2018-02-27 11:44:41', '2018-02-27 11:45:52');
INSERT INTO `partidas_apr` VALUES ('110', '0', '4', '2018-02-27 11:45:37', '2018-02-27 11:50:13');
INSERT INTO `partidas_apr` VALUES ('111', '0', '4', '2018-02-27 11:50:30', '2018-02-27 11:52:01');
INSERT INTO `partidas_apr` VALUES ('112', '0', '4', '2018-02-27 11:52:11', '2018-02-27 11:58:47');
INSERT INTO `partidas_apr` VALUES ('113', '0', '4', '2018-02-27 11:58:58', '2018-02-27 11:59:31');
INSERT INTO `partidas_apr` VALUES ('114', '0', '4', '2018-02-27 11:59:44', '2018-02-27 12:12:09');
INSERT INTO `partidas_apr` VALUES ('115', '0', '4', '2018-02-27 12:00:56', '2018-02-27 12:12:09');
INSERT INTO `partidas_apr` VALUES ('116', '0', '4', '2018-02-27 12:12:19', '2018-02-27 12:15:16');
INSERT INTO `partidas_apr` VALUES ('117', '0', '4', '2018-02-27 12:12:27', '2018-02-27 12:15:16');
INSERT INTO `partidas_apr` VALUES ('118', '0', '4', '2018-02-27 12:15:26', '2018-02-27 12:17:32');
INSERT INTO `partidas_apr` VALUES ('119', '0', '4', '2018-02-27 12:15:35', '2018-02-27 12:17:32');
INSERT INTO `partidas_apr` VALUES ('120', '0', '4', '2018-02-27 12:17:44', '2018-02-27 12:20:11');
INSERT INTO `partidas_apr` VALUES ('121', '0', '4', '2018-02-27 12:17:53', '2018-02-27 12:20:11');
INSERT INTO `partidas_apr` VALUES ('122', '0', '4', '2018-02-27 12:20:21', '2018-02-27 12:21:56');
INSERT INTO `partidas_apr` VALUES ('123', '0', '4', '2018-02-27 12:20:32', '2018-02-27 12:21:56');
INSERT INTO `partidas_apr` VALUES ('124', '0', '4', '2018-02-27 12:22:06', '2018-02-27 12:23:35');
INSERT INTO `partidas_apr` VALUES ('125', '0', '4', '2018-02-27 12:22:14', '2018-02-27 12:23:36');
INSERT INTO `partidas_apr` VALUES ('126', '0', '4', '2018-02-27 12:23:59', '2018-02-27 12:25:16');
INSERT INTO `partidas_apr` VALUES ('127', '0', '4', '2018-02-27 12:24:14', '2018-02-27 12:25:16');
INSERT INTO `partidas_apr` VALUES ('128', '0', '4', '2018-02-27 12:25:32', '2018-02-27 13:30:22');
INSERT INTO `partidas_apr` VALUES ('129', '0', '4', '2018-02-27 12:25:40', '2018-02-27 13:30:22');
INSERT INTO `partidas_apr` VALUES ('130', '0', '4', '2018-02-27 13:39:04', '2018-02-27 13:41:18');
INSERT INTO `partidas_apr` VALUES ('131', '0', '4', '2018-02-27 13:41:29', '2018-02-27 13:49:39');
INSERT INTO `partidas_apr` VALUES ('132', '0', '4', '2018-02-27 13:49:50', '2018-02-27 13:51:34');
INSERT INTO `partidas_apr` VALUES ('133', '0', '4', '2018-02-27 13:51:45', '2018-02-27 13:52:55');
INSERT INTO `partidas_apr` VALUES ('134', '0', '4', '2018-02-27 13:53:04', '2018-02-27 13:53:55');
INSERT INTO `partidas_apr` VALUES ('135', '0', '4', '2018-02-27 13:54:04', '2018-02-28 10:13:11');
INSERT INTO `partidas_apr` VALUES ('136', '0', '4', '2018-02-28 10:28:26', '2018-02-28 10:28:40');
INSERT INTO `partidas_apr` VALUES ('137', '0', '4', '2018-02-28 10:29:15', '2018-02-28 10:30:19');
INSERT INTO `partidas_apr` VALUES ('138', '0', '4', '2018-02-28 10:30:05', '2018-02-28 10:38:46');
INSERT INTO `partidas_apr` VALUES ('139', '0', '4', '2018-02-28 10:38:22', '2018-02-28 10:43:40');
INSERT INTO `partidas_apr` VALUES ('140', '0', '4', '2018-02-28 10:43:25', '2018-02-28 10:46:12');
INSERT INTO `partidas_apr` VALUES ('141', '0', '4', '2018-02-28 10:45:58', '2018-02-28 10:46:58');
INSERT INTO `partidas_apr` VALUES ('142', '0', '4', '2018-02-28 10:46:41', '2018-02-28 10:46:58');
INSERT INTO `partidas_apr` VALUES ('143', '0', '4', '2018-02-28 11:00:08', null);
INSERT INTO `partidas_apr` VALUES ('144', '0', '4', '2018-02-28 11:08:19', null);
INSERT INTO `partidas_apr` VALUES ('145', '0', '4', '2018-02-28 13:43:47', null);
INSERT INTO `partidas_apr` VALUES ('146', '0', '4', '2018-02-28 13:48:30', null);
INSERT INTO `partidas_apr` VALUES ('147', '0', '4', '2018-02-28 13:49:33', null);
INSERT INTO `partidas_apr` VALUES ('148', '1', '4', '2018-03-13 15:58:47', null);
INSERT INTO `partidas_apr` VALUES ('149', '1', '4', '2018-03-20 09:08:01', null);
INSERT INTO `partidas_apr` VALUES ('150', '1', '4', '2018-03-20 09:52:15', null);
INSERT INTO `partidas_apr` VALUES ('151', '1', '4', '2018-04-16 09:01:22', null);
INSERT INTO `partidas_apr` VALUES ('152', '0', '4', '2018-04-16 09:01:55', null);
INSERT INTO `partidas_apr` VALUES ('153', '0', '4', '2018-04-16 09:12:54', null);
INSERT INTO `partidas_apr` VALUES ('154', '0', '4', '2018-04-16 10:23:01', null);
INSERT INTO `partidas_apr` VALUES ('155', '1', '23', '2018-07-12 12:23:17', null);
INSERT INTO `partidas_apr` VALUES ('156', '1', '23', '2018-07-13 13:50:21', null);
INSERT INTO `partidas_apr` VALUES ('157', '1', '4', '2018-07-30 16:01:24', null);
INSERT INTO `partidas_apr` VALUES ('158', '1', '4', '2018-07-30 16:01:40', null);
INSERT INTO `partidas_apr` VALUES ('159', '1', '4', '2018-07-30 16:01:57', null);
INSERT INTO `partidas_apr` VALUES ('160', '1', '4', '2018-07-30 16:02:11', null);
INSERT INTO `partidas_apr` VALUES ('161', '1', '4', '2018-07-30 16:02:35', null);
INSERT INTO `partidas_apr` VALUES ('162', '1', '4', '2018-07-30 16:03:24', null);
INSERT INTO `partidas_apr` VALUES ('163', '1', '4', '2018-07-30 16:06:40', null);
INSERT INTO `partidas_apr` VALUES ('164', '1', '4', '2018-07-30 16:25:41', null);
INSERT INTO `partidas_apr` VALUES ('165', '1', '4', '2018-07-31 13:13:20', null);
INSERT INTO `partidas_apr` VALUES ('166', '1', '4', '2018-07-31 13:15:39', null);
INSERT INTO `partidas_apr` VALUES ('167', '0', '4', '2018-07-31 13:17:15', null);
INSERT INTO `partidas_apr` VALUES ('168', '0', '4', '2018-07-31 13:32:23', null);
INSERT INTO `partidas_apr` VALUES ('169', '1', '4', '2018-09-14 14:07:17', null);
INSERT INTO `partidas_apr` VALUES ('170', '1', '4', '2018-09-14 14:08:15', null);
INSERT INTO `partidas_apr` VALUES ('171', '1', '4', '2018-09-14 14:19:05', null);
INSERT INTO `partidas_apr` VALUES ('172', '1', '4', '2019-01-22 13:49:25', null);

-- ----------------------------
-- Table structure for partidas_juzgado
-- ----------------------------
DROP TABLE IF EXISTS `partidas_juzgado`;
CREATE TABLE `partidas_juzgado` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cantidad` int(11) NOT NULL,
  `ingresado_por` int(11) NOT NULL,
  `fecha_ingreso` datetime NOT NULL,
  `fecha_vacio` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of partidas_juzgado
-- ----------------------------
INSERT INTO `partidas_juzgado` VALUES ('1', '-6', '1', '2018-01-31 11:06:45', null);
INSERT INTO `partidas_juzgado` VALUES ('2', '1', '4', '2018-01-31 11:22:07', null);
INSERT INTO `partidas_juzgado` VALUES ('3', '3', '4', '2018-01-31 11:25:40', null);
INSERT INTO `partidas_juzgado` VALUES ('4', '0', '4', '2018-02-08 12:43:13', '2018-02-08 12:43:32');
INSERT INTO `partidas_juzgado` VALUES ('5', '0', '4', '2018-03-13 14:57:25', null);
INSERT INTO `partidas_juzgado` VALUES ('6', '0', '4', '2018-03-19 12:53:01', null);
INSERT INTO `partidas_juzgado` VALUES ('7', '1', '4', '2018-03-20 09:24:32', null);
INSERT INTO `partidas_juzgado` VALUES ('8', '2', '4', '2018-03-20 09:24:36', null);
INSERT INTO `partidas_juzgado` VALUES ('9', '1', '4', '2018-03-20 09:40:42', null);
INSERT INTO `partidas_juzgado` VALUES ('10', '1', '4', '2018-03-20 09:46:56', null);
INSERT INTO `partidas_juzgado` VALUES ('11', '2', '4', '2018-03-20 09:50:31', null);
INSERT INTO `partidas_juzgado` VALUES ('12', '2', '4', '2018-03-20 09:51:16', null);
INSERT INTO `partidas_juzgado` VALUES ('13', '1', '4', '2018-03-20 09:55:43', null);
INSERT INTO `partidas_juzgado` VALUES ('14', '0', '4', '2018-03-21 09:09:03', null);
INSERT INTO `partidas_juzgado` VALUES ('15', '0', '4', '2018-03-21 09:11:19', null);
INSERT INTO `partidas_juzgado` VALUES ('16', '0', '4', '2018-03-21 09:24:08', null);
INSERT INTO `partidas_juzgado` VALUES ('17', '0', '4', '2018-03-21 09:24:14', null);
INSERT INTO `partidas_juzgado` VALUES ('18', '0', '4', '2018-03-21 09:25:08', null);
INSERT INTO `partidas_juzgado` VALUES ('19', '0', '4', '2018-03-21 09:25:27', null);
INSERT INTO `partidas_juzgado` VALUES ('20', '0', '4', '2018-03-21 09:26:09', null);
INSERT INTO `partidas_juzgado` VALUES ('21', '0', '4', '2018-03-21 09:26:28', null);
INSERT INTO `partidas_juzgado` VALUES ('22', '0', '4', '2018-03-21 09:27:30', null);
INSERT INTO `partidas_juzgado` VALUES ('23', '0', '4', '2018-03-21 09:27:55', null);
INSERT INTO `partidas_juzgado` VALUES ('24', '1', '4', '2018-03-21 09:58:47', null);
INSERT INTO `partidas_juzgado` VALUES ('25', '0', '4', '2018-04-16 09:11:51', null);
INSERT INTO `partidas_juzgado` VALUES ('26', '0', '4', '2018-04-16 09:12:12', null);
INSERT INTO `partidas_juzgado` VALUES ('27', '0', '4', '2018-04-16 10:24:42', null);
INSERT INTO `partidas_juzgado` VALUES ('28', '1', '4', '2018-08-06 14:37:09', null);
INSERT INTO `partidas_juzgado` VALUES ('29', '1', '4', '2018-08-06 14:39:15', null);
INSERT INTO `partidas_juzgado` VALUES ('30', '1', '4', '2018-08-06 14:41:03', null);
INSERT INTO `partidas_juzgado` VALUES ('31', '1', '4', '2018-08-06 14:43:59', null);
INSERT INTO `partidas_juzgado` VALUES ('32', '1', '4', '2018-08-06 14:44:16', null);
INSERT INTO `partidas_juzgado` VALUES ('33', '1', '4', '2018-08-06 15:24:16', null);
INSERT INTO `partidas_juzgado` VALUES ('34', '1', '4', '2018-08-06 15:26:58', null);
INSERT INTO `partidas_juzgado` VALUES ('35', '1', '4', '2018-08-06 15:27:18', null);
INSERT INTO `partidas_juzgado` VALUES ('36', '1', '4', '2018-08-06 15:27:42', null);

-- ----------------------------
-- Table structure for productos
-- ----------------------------
DROP TABLE IF EXISTS `productos`;
CREATE TABLE `productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nro_serie` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo` int(11) DEFAULT NULL,
  `descripcion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modelo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `partida_id` int(11) NOT NULL,
  `ticket` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disponible` int(11) NOT NULL,
  `operativo` int(11) NOT NULL,
  `origen` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ubicacion` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `departamento_id` int(11) DEFAULT NULL,
  `comentarios` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_767490E65A91C08D` (`departamento_id`),
  KEY `IDX_767490E6702D1D47` (`tipo`),
  CONSTRAINT `FK_767490E65A91C08D` FOREIGN KEY (`departamento_id`) REFERENCES `departamentos` (`id`),
  CONSTRAINT `FK_767490E6702D1D47` FOREIGN KEY (`tipo`) REFERENCES `tipos_productos` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1108 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of productos
-- ----------------------------
INSERT INTO `productos` VALUES ('100', 'LE15HXBY129666B', '3', 'Monitor TRC Samsung  Sync Master  15\"', '591 s S', '1', '', '0', '1', 'Donacion de Personal', 'MESA 8', '1', null);
INSERT INTO `productos` VALUES ('101', 'BRA518016T', '3', 'HP Tipo TRC  15\"', 'PE 1154', '1', '', '0', '1', 'Oficina de Coordinación', 'MESA 8', '1', 'Lo dieron para armar kit de Reciclado\r\n');
INSERT INTO `productos` VALUES ('102', '8749004246', '3', 'Monitor TRC Kelyx   15\"', 'JD14416', '1', '121', '0', '1', 'Donación Torre 1  18/5/17', 'MESA 8', '1', '');
INSERT INTO `productos` VALUES ('103', '', '5', 'Gabinete completo p kit reciclado. Coradir. S/Memoria. Dual Core. Disco SATA 80G', 'Mother: Asrock WolfDale 1333 D667', '1', '', '0', '0', '', 'Estanteria (N°5), Est. 1', '1', '');
INSERT INTO `productos` VALUES ('104', 'SC15H9LL915728Z', '3', 'Monitor TRC Samsung  Sync Master  15\"', 'PN15VS', '1', '121', '0', '1', 'Donación Torre 1  18/5/17', 'MESA 8', '1', null);
INSERT INTO `productos` VALUES ('105', '', '5', 'Gabinete p repuesto o kit reciclado. Con Mother, micro y Fuente. Sin Disco', 'Mother: Asrock K7VM3', '1', '121', '0', '1', 'Donación Torre 1  18/5/17', 'Estanteria (N°5), Est. 1', '1', '');
INSERT INTO `productos` VALUES ('106', 'LB15HXAL203015W', '3', 'Monitor TRC Samsung  Sync Master  15\"', '591 s S', '1', '', '0', '1', 'Donacion de Personal', 'MESA 8', '1', null);
INSERT INTO `productos` VALUES ('107', 'LB15HXBY310619A', '3', 'Monitor TRC Samsung  Sync Master  15\"', '591 s S', '1', '', '0', '1', 'Donacion de Personal', 'MESA 8', '1', null);
INSERT INTO `productos` VALUES ('108', 'LB15HXBY820698T', '3', 'Monitor TRC Samsung  Sync Master  15\"', '591 s S', '1', '', '0', '1', 'Donacion de Personal', 'MESA 8', '1', null);
INSERT INTO `productos` VALUES ('109', 'C100494142', '3', 'Monitor TRC View Sonic E40   15\"', 'VCDTS21384-1M', '1', '', '1', '0', 'Donacion de Personal', 'MESA 8', '1', null);
INSERT INTO `productos` VALUES ('110', 'SC15H9LL915281R', '3', 'Monitor TRC Samsung  Sync Master  15\"', '592 V [R] S', '1', '', '1', '1', 'Donacion de Personal', 'MESA 8', '1', null);
INSERT INTO `productos` VALUES ('111', 'LB15HXAL203001M', '3', 'Monitor TRC Samsung  Sync Master  15\"', '591 s S', '1', '', '0', '1', 'Donacion de Personal', 'MESA 8', '1', null);
INSERT INTO `productos` VALUES ('112', 'CNC4510JQG', '3', 'Monitor TRC HP    15\"', 'HSTND-1A05', '1', '405', '0', '0', 'Dirección de Empleo', 'MESA 8', '1', 'Vino con un CPU. El T está cerrado sin solución\r\n');
INSERT INTO `productos` VALUES ('113', 'SC15H9LL914965Y', '3', 'Monitor TRC Samsung  Sync Master  15\"', '592 V [R] S', '1', '', '1', '1', 'Donacion de Personal', 'MESA 8', '1', null);
INSERT INTO `productos` VALUES ('114', '', '5', 'Gabinete completo. Con fuente. Pentium 4. Con memoria DDR 400. S/disco', 'Mother Asrock 775i 65G', '1', '', '1', '0', 'Donacion de Personal', 'Estanteria (N°5), Est. 1', '1', 'Enciende pero no da video. Da olor a quemado. Le falta una tapa.');
INSERT INTO `productos` VALUES ('115', 'MXJ8100BXM', '5', 'CPU HP p repuesto. Compaq dc5700 Microtower.  C/fuente. S/ memorias. S/disco. Pentium E2160 Dual Core', 'Compaq dc5700 Microtower', '1', '', '1', '0', 'Interno Tecnología', 'Estanteria (N°5), Est. 2', '1', 'No le funciona el video, ni con placa externa. Tenía disco roto. P repuesto.');
INSERT INTO `productos` VALUES ('116', 'BRA518014T', '3', 'Monitor TRC HP    15\"', 'PE 1154', '1', '', '1', '1', '', 'MESA 7, abajo', '1', null);
INSERT INTO `productos` VALUES ('117', '29024472', '3', 'Monitor TRC ProView   15\"', 'PX 558', '1', '', '1', '0', '', 'MESA 7, abajo', '1', null);
INSERT INTO `productos` VALUES ('118', '', '5', 'CPU SFX. Para reciclar.  C/fuente. S/Memorias.  C/ disco 320G. Procesador AMD Athlon II.', 'Mother Biostar N68S3+ Ver 6.0', '1', '202', '1', '1', 'Coordinación 13 y 50', 'Estanteria (N°5), Est. 2', '1', '');
INSERT INTO `productos` VALUES ('119', '', '5', 'CPU Para reciclar.  C/fuente ATX 350W. S/ memoria. C/ disco IDE 40G IBM. Procesador Pentium IV. ', '', '1', '', '1', '1', 'Donación de Personal', 'Estanteria (N°5), Est. 2', '1', '');
INSERT INTO `productos` VALUES ('120', '', '5', 'CPU P reciclar. Wind XP. C/fuente. C/2 memorias DDR 400 de 512MB. C/ disco Samsung SATA 80G. Proc Pentium IV. ', 'Mother Asrock 775i 65G Presler Conroe', '1', '', '1', '1', 'Donación de Personal', 'Estanteria (N°5), Est. 1', '1', 'Tiene Flopi de 3 1/2 y lectograbadora de CD.');
INSERT INTO `productos` VALUES ('121', '', '5', 'CPU. Wind XP. C/fuente. C/2 memorias DDR 400 de 512MB. C/ disco IDE  80G. Proc. Intel Celeron D330. Disco 3 y 1/2 y CD.', 'Mother Asus PSV8000-MX', '1', '', '1', '1', 'Donación de Personal', 'Estanteria (N°5), Est. 2', '1', 'Bootea XP.');
INSERT INTO `productos` VALUES ('122', 'SN114350100380', '5', 'CPU Compacto marca CX Slim. CPU AMD Sempron145 AM3. HD Sata 320G. Mem DDR3 2G 1333Mhz. Graba DVD Sony', 'Mother Gigabyte AM3 GAM 68MTSZ', '1', '339', '1', '0', 'Obras Particulares 3/4/17', 'Estanteria (N°5), Est. 2', '1', 'Tiene la fuente quemada. El Tiket esta pendiente a la espera que la compren.');
INSERT INTO `productos` VALUES ('123', '210231A467X077000008', '4', 'Router HUAWEI Quidway AR 18-20S.  4 ptos LAN, 1Console, 1WAN.', 'Quidway AR 18-20S', '1', '', '1', '1', 'Dirección Provincial de Telecomunicaciones', 'Estanteria (N°5), Est. 1', '1', '');
INSERT INTO `productos` VALUES ('124', '219801A03BC066000098', '8', 'Switch HUAWEI Quidway S2000Series.  24 ptos.  1Console.', 'Quidway S2403H-HI', '1', '', '1', '1', 'Dirección Provincial de Telecomunicaciones', 'Estanteria (N°5), Est. 1', '1', '');
INSERT INTO `productos` VALUES ('125', '10V004600484', '3', 'Monitor TRC View Sonic E50 15\"', 'VCDTS21915-2M', '1', '303', '1', '0', 'Niñez y Adolescencia', 'MESA 7, abajo', '1', 'Son 3 monitores en total. Se les respondió el T diciendo que les ofrecemos otro TRC a cambio. No respondieron.');
INSERT INTO `productos` VALUES ('126', '8XK62P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 441826. Por ahora se encuentra en el call center p una  prueba.');
INSERT INTO `productos` VALUES ('127', 'DT7Q5P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 442033.');
INSERT INTO `productos` VALUES ('128', 'B59HYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467517.');
INSERT INTO `productos` VALUES ('129', '7WP4DQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467679.');
INSERT INTO `productos` VALUES ('130', 'GSP4DQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 442051.');
INSERT INTO `productos` VALUES ('131', 'B96GYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467594.');
INSERT INTO `productos` VALUES ('132', 'BWK62P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 441738.');
INSERT INTO `productos` VALUES ('133', 'DWGP5M1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 460735 ATENCION: NO SE SI ES ESTA MAQUINA. ESTUDIAR CUAL FUE, POR DESCARTE. Por que se traspapeló el N de máquina que en realidad se mandó.');
INSERT INTO `productos` VALUES ('134', '70HTBQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 463749.');
INSERT INTO `productos` VALUES ('135', 'B8JGYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467566.');
INSERT INTO `productos` VALUES ('136', 'B5MFYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467593.');
INSERT INTO `productos` VALUES ('137', 'JSWJKQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 442157.');
INSERT INTO `productos` VALUES ('138', '62V92P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467250');
INSERT INTO `productos` VALUES ('139', 'B8HGYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467546.');
INSERT INTO `productos` VALUES ('140', '8T7Q5P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 441996.');
INSERT INTO `productos` VALUES ('141', 'FSK62P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 441736.');
INSERT INTO `productos` VALUES ('142', 'B7NHYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467292.');
INSERT INTO `productos` VALUES ('143', 'FKXJKQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 442158.');
INSERT INTO `productos` VALUES ('144', 'F2XJKQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 442134.');
INSERT INTO `productos` VALUES ('145', 'B7MFYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467284.');
INSERT INTO `productos` VALUES ('146', 'B5NGYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467287.');
INSERT INTO `productos` VALUES ('147', '6XGP5M1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 460856.');
INSERT INTO `productos` VALUES ('148', 'B7LLYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467280.');
INSERT INTO `productos` VALUES ('149', 'CZGP5M1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 460888.');
INSERT INTO `productos` VALUES ('150', 'BBJDYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '1', '', '0', '1', 'Donacion de YPF', 'MESA 7, arriba', '1', 'N° interno de YPF: 467571 .');
INSERT INTO `productos` VALUES ('151', 'HH5306C070358', '3', 'Monitor TRC e-View 17\"', '775CE', '1', '303', '1', '0', 'Niñez y Adolescencia', 'MESA 8, arriba', '1', 'Son 3 monitores en total. Se les respondió el T diciendo que les ofrecemos otro TRC a cambio. No respondieron.');
INSERT INTO `productos` VALUES ('152', '', '9', 'Caja con fuentes de Switching para reparar/repuesto. Hay de PC, Monitor, Servidores, Switch, etc.', '', '1', '', '1', '0', '', 'MESA 7, arriba', '1', null);
INSERT INTO `productos` VALUES ('153', '', '10', 'Caja con Placas Madre para repuesto o funcionando. ', '', '1', '', '1', '1', '', 'MESA 7, arriba', '1', null);
INSERT INTO `productos` VALUES ('154', '', '5', 'Gabinete Coradir sin mother ni fuente. Con disketera 3 1/2 y lectora CD.', '', '1', '', '1', '1', 'Resago del Taller', 'Estanteria (N°5), Est. 3', '1', '');
INSERT INTO `productos` VALUES ('155', '', '5', 'Gabinete Coradir sin mother ni fuente. Con disketera 3 1/2 y lectora CD.', '', '1', '', '1', '1', 'Resago del Taller', 'Estanteria (N°5), Est. 3', '1', '');
INSERT INTO `productos` VALUES ('156', '', '5', 'Gabinete Coradir sin mother ni fuente. Con disketera 3 1/2 y lectora CD.', '', '1', '', '1', '1', 'Resago del Taller', 'Estanteria (N°5), Est. 3', '1', '');
INSERT INTO `productos` VALUES ('157', '', '5', 'Gabinete Codegen sin mother ni fuente. Con disketera 3 1/2.', '', '1', '', '1', '1', 'Resago del Taller', 'Estanteria (N°5), Est. 3', '1', '');
INSERT INTO `productos` VALUES ('158', '808SPFXA2187', '3', 'Monitor LG LCD Flatron', 'W1942S - PF', '1', '21', '1', '0', 'APR Area Impresión', 'MESA 7, arriba', '1', 'No enciende. P repuestos. Se realizó prueba con placa de señal de otro equipo y funciona. Por lo tanto la placa de potencia esta buena.');
INSERT INTO `productos` VALUES ('159', '203NSQP1V164', '3', 'Monitor LG LCD Flatron', 'E1941S - BNX', '1', '', '1', '0', '', 'MESA 7, arriba', '1', 'P repuestos.');
INSERT INTO `productos` VALUES ('160', '', '11', 'Caja con lectoras de CD/DVD y  de 3 1/2', '', '1', '', '1', '1', '', 'MESA 7, arriba', '1', null);
INSERT INTO `productos` VALUES ('161', '', '5', 'Gabinete vacío  V8 Multimedia Computer Sistem. C/ lectora de 3 1/2 y panel usb frontal ', '', '1', '', '1', '1', '', 'Estanteria (N°5), Est. 1', '1', '');
INSERT INTO `productos` VALUES ('162', '', '5', 'Gabinete vacío SFX tecnology.', '', '1', '', '1', '1', '', '', '1', 'Tiene escrito APR 0007');
INSERT INTO `productos` VALUES ('163', '', '2', 'Caja con teclados que no funcionan pero pueden repararse o usarse de repuesto', '', '1', '', '1', '0', '', 'MESA 7', '1', null);
INSERT INTO `productos` VALUES ('164', '', '5', 'Gabinete vacío con lectora de CD y disquetera de 3 y 1/2', '', '1', '', '1', '1', 'Asesoría 1', 'MESA 7', '1', null);
INSERT INTO `productos` VALUES ('165', '', '5', 'Gabinete vacío con lectora de CD y disquetera de 3 y 1/2', '', '1', '', '1', '1', '', 'Estanteria (N°5), Est. 3', '1', '');
INSERT INTO `productos` VALUES ('166', '', '5', 'Gabinete vacío con lectora de CD y disquetera de 3 y 1/2', '', '1', '', '1', '1', 'Terminal?', 'MESA 7', '1', null);
INSERT INTO `productos` VALUES ('167', 'CNC4500SQ2', '3', 'Monitor TRC HP    15\"  5502. Enciende pero al conectar el cable VGA se descontrola.', 'HSTND-1A05', '1', '303', '1', '0', 'Niñez y Adolescencia', 'MESA 8', '1', 'Son 3 monitores en total. Se les respondió el T diciendo que les ofrecemos otro TRC a cambio. No respondieron');
INSERT INTO `productos` VALUES ('168', '', '5', 'CPU con Gabinete Magnum Tech. Pertenece al Juzgado. Espera por Placa de Video', '', '1', '702', '1', '0', 'Juzgado de Faltas', 'Estanteria (N°5), Est. 1', '1', 'El Ticket correspondía a 3 PC, dos se repararon y ésta necesita placa video externa. Se efectuará pedido a Diego V. p que la compre.');
INSERT INTO `productos` VALUES ('169', '', '5', 'Gabinete SFX  G543NE c fuente (funciona ok) y Mother Asus A7S8X-MX. Tiene dos Memorias de 256 y 128MB.', 'A7S8X-MX', '1', '', '1', '0', 'APR', 'Estanteria (N°5), Est. 2', '1', 'Afuera dice APR 0165');
INSERT INTO `productos` VALUES ('170', '', '5', 'Gabinete  CX. Espera compra de disco por parte de ellos.', 'GAB CX 727-14 500W B', '1', '2016080310000011', '1', '0', 'Contribuyentes Especiales (Adriana Espinosa)', 'Estanteria (N°5), Est. 4', '1', 'El T tiene más de 490 días de antigüedad. Se va a reiterar el pedido y si no que la vengan a retirar.');
INSERT INTO `productos` VALUES ('171', '', '5', 'Gabinete Eurocase. Placa GygaByte. Proc Pentium Dual C. E5500 2.8GHz. Memoria DDR3 2GB. HD 320GB Tiene  Win7  32bit.', 'Mother GA-G41MT-S2', '1', '', '0', '1', 'APR', 'Estanteria (N°5), Est. 4', '1', 'Para armar KIT DE RECICLADO');
INSERT INTO `productos` VALUES ('172', '', '5', 'Gabinete BRB AMD Sempron. (afuera dice CPU 0092)', '', '1', '103', '1', '0', 'Direccion de Catastro', 'MESA 7, superior', '1', 'El T es del 4/10/2016. Catastro. Espera por compra de HD. Se reitera pedido. Contacto: 4295389 Sandra García.');
INSERT INTO `productos` VALUES ('173', '', '5', 'Gabinete Mustiff.', '', '1', '184', '1', '0', 'Juzgado de Faltas, Dr Rusconi', 'MESA 7, superior', '1', 'Espera compra de HD desde hace casi 600d. Se reiterará pedido.');
INSERT INTO `productos` VALUES ('174', '', '5', 'Gabinete Magnum Tech', 'CX831', '1', '279', '1', '0', 'Departamento Ingresos', 'MESA 7, superior', '1', 'Espera compra de HD desde hace casi 590d. Se reiterará pedido.');
INSERT INTO `productos` VALUES ('175', 'SN-1895', '12', 'Adaptador para la Central Telefónica.Con su fuente 5V 2.5A.  Marca RedFone Communications. FoneBRIDGE2-Single', '750-4000', '1', '', '1', '0', 'APR', 'Armario Met. N°6, 3er estante', '1', null);
INSERT INTO `productos` VALUES ('176', '931BB46MF037', '3', 'Monitor TRC Compaq. Funciona OK. Tal vez despues de un rato de andar se obscurece un poco.', 'V510b', '1', '13041 y 12821', '1', '1', 'Archivo General', 'MESA 8, superior', '1', 'Pidieron uno de reemplazo mediante T°12821: se les dio el AP101RJ.  Luego trajeron este p reparar por T°13041. Afuera tiene escrito SYS0910200812');
INSERT INTO `productos` VALUES ('177', 'S041H9HC508916Y', '3', 'Monitor LED Samsung 19\" VGA y DVI. Con su fuente.', 'S19B300B', '1', '', '1', '1', 'APR Call', 'Armario Met. N°6, superior', '1', 'Tiene una etiqueta que dice \'call212  Clave:kws578\' ');
INSERT INTO `productos` VALUES ('178', '102NSNS0G935', '3', 'Monitor LCD LG FLATRON 19\" VGA y DVI. Sin la fuente.', 'W1943TE-PFV', '1', '', '1', '1', 'APR Tecnología', 'Armario Met. N°6, superior', '1', 'Nota: pertenecía a Mauro. Enciende. Tiene problemas de parpadeo, en realidad así como esta no puede usarse. Sería p reparar o repuesto');
INSERT INTO `productos` VALUES ('179', '312NSZF1J389', '3', 'Monitor LED LG 19\" sólo VGA. Con su fuente 19V 1.2A', '19 EN 33', '1', '', '1', '1', 'Desconocido (problablemente del Call)', 'Armario Met. N°6, superior', '1', null);
INSERT INTO `productos` VALUES ('180', 'MY19H9NSA25870N', '3', 'Monitor LCD Samsung 19\" alimentacion 220VAC. Sólo VGA.', '943SNXPLUS', '1', '', '0', '1', '', 'Armario Met. N°6, superior', '1', 'Tiene una raya vertical sobre el lado izquierdo de la pantalla, pero en la mayoría de los casos ni se nota. Como back up puede usarse perfecto.');
INSERT INTO `productos` VALUES ('181', '029SH9XJ203199V', '3', 'Monitor LED Samsung 19\" VGA y HDMI. Con  su fuente 14V 1.072A', 'LS19D300HYCZB', '1', '', '0', '1', '', 'MESA 7, superior', '1', 'Tiene una etiqueta con el N° 10.');
INSERT INTO `productos` VALUES ('182', 'PE17HVDP902441F', '3', 'Monitor LCD Samsung de pantalla cuadrada. Alimentacion 220. Solo VGA.', '732N Plus', '1', '', '1', '0', '', 'Armario Met. N°6, superior', '1', 'Enciende, pero no hay imagen. Pendiente intentar repararlo. Si no para repuestos.');
INSERT INTO `productos` VALUES ('183', '010UXNU5D231', '3', 'Monitor LG LCD 19\". VGA. Alimentación 220 VAC.', 'Flatron W1943SS-PF', '1', '', '1', '0', 'APR Call', 'Armario Met. N°6, superior', '1', 'No enciende. En este modelo suelen quemarse los capac. de fuente, pendiente reparar. Afuera dice   \'APR0036\'   y   \'call204  twk564\'.');
INSERT INTO `productos` VALUES ('184', '808SPQJA4596', '3', 'Monitor LG LCD 19\". VGA. Alimentación 220 VAC.', 'Flatron W1942S-PF', '1', '', '0', '1', '', 'Armario Met. N°6, superior', '1', 'Tiene una etiqueta de codigo de barra de la Municipalidad que dice 0000057361');
INSERT INTO `productos` VALUES ('185', '0J9235-64180-7AF-027P', '3', 'Monitor TRC Dell 17\". Negro. Enciende, pero no procesa la señal; y la pantalla tiene medio mal los colores. ', 'E 773C', '3', '', '1', '0', 'Interno Taller', 'Mesa7, inferior', '1', 'Enciende bien pero no procesa la señal, tal vez sea el cable. La  pantalla no tiene del todo bien los colores. ');
INSERT INTO `productos` VALUES ('186', 'EW D8200042932', '5', 'Gabinete con Mother y fuente. Sin Disco ni Lectoras. Procesador  Intel Celeron 1,8GHz. Mother Biostar', 'Mother P4M900-M7SE. Ver 7.1', '4', '', '1', '0', '', 'Estanteria (N°5), Est. 1', '1', 'Tiene algunos capacitores hinchados, cambiarlos si o si.');
INSERT INTO `productos` VALUES ('187', 'CNCJ261920', '13', 'Impresora HP LaserJet 1000 Series. Conección por puerto paralelo. Posee un adaptador a USB. Para repuestos', 'Q1342A', '5', '', '1', '0', '', 'Mesa2, arriba', '1', 'Estaba en el taller hace varios años. No se sabe si funciona bien, pero es muy difícil ponerla en marcha, por los drivers. El origen es desconocido por que se extravió el papel que tenía. ');
INSERT INTO `productos` VALUES ('188', '', '5', 'Gabinete Neo Light, muy viejo, todo obsoleto. Fuente AT, disco, mother, lectora CD, lectora 3 y 1/2.  Se pueden sacar los coolers, fichas y cables', '', '6', '', '1', '0', 'Interno APR, Chuchu', 'Mesa8, superior', '1', 'Tiene etiqueta que dice LOLINGO (Chuchu) 11/9/13. Gabinete pesado y obsoleto, al igual que todos sus componentes ');
INSERT INTO `productos` VALUES ('189', '', '5', 'Gabinete marca Blaze. Pertenece a Espacios Verdes, del 10/05.', '', '7', '083 y 051', '1', '0', 'Espacios Verdes', 'Mesa8, superior', '1', 'Se fusionaron los T, creyendo que eran dos T de la misma máquina. De todas maneras no se puedo instalar SO, hay alguna falla en Mother o Disco. Nunca más preguntaron o vinieron por la máquina.');
INSERT INTO `productos` VALUES ('190', '', '5', 'Gabinete MagnumTech CX 831. P\' reciclar. C/ Mother, Memoria DDR3 2GB, Fuente y micro AMD Athlon2, ADX2500CK23GM. Lector MultiMemorias ', 'Mother Biostar N68S3+ Ver 6.0', '7', '', '0', '1', '', 'Mesa7, superior', '1', 'Es para reciclar o sacarle cosas. Tiene una etiqueta que dice \"Mun de L P, PC Pentium E2160 Dual Core. 0000057349\".  Tiene lector multimemoria, SD, micro SD, etc.');
INSERT INTO `productos` VALUES ('191', '', '5', 'Gabinete Performance. Mother AsRock. HD Samsung 250G. Sin Memoria. Proc AMD Athlon2 ADX2400CK23GQ. Fuente y lectora de DVD.', 'Mother AsRock N61P-S', '7', '', '0', '1', 'Interno, Call. Claudio Taini', 'Mesa8, superior', '1', 'Era de Claudio Taini. No anda VGA onboard, solo PCI Express. Antes de usarla consultar.');
INSERT INTO `productos` VALUES ('192', null, '17', 'Paquete juzgado 1', '', '28', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('193', null, '17', 'asdasd', '', '29', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('194', null, '17', 'con imagen', '', '30', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('195', null, '17', 'con img 2', '', '32', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('196', null, '17', 'Paquete prueba devolución', '', '33', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('197', null, '17', 'Paquete prueba devolución', '', '34', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('198', null, '17', 'Pquete prueba devolucion', '', '35', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('199', null, '17', 'Pquete prueba devolucion', '', '36', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1000', 'ZZQFH4ZJ600353', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1001', 'ZZQFH4ZJ601114', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1002', 'ZZQFH4ZJ601116', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1003', 'ZZQFH4ZJ601117', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1004', 'ZZQFH4ZJ601522', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1005', 'ZZQFH4ZJ600742', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1006', 'ZZQFH4ZJ601118', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1007', 'ZZQFH4ZJ600345', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1008', 'ZZQFH4ZJ600736', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1009', 'ZZQFH4ZJ601515', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1010', 'ZZQFH4ZJ600346', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1011', 'ZZQFH4ZJ601518', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1012', 'ZZQFH4ZJ600352', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1013', 'ZZQFH4ZJ600738', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1014', 'ZZQFH4ZJ601107', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1015', 'ZZQFH4ZJ601110', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1016', 'ZZQFH4ZJ600734', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '1', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1017', 'ZZQFH4ZJ600351', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1018', 'ZZQFH4ZJ600347', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '1', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1019', 'ZZQFH4ZJ601519', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '1', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1020', 'ZZQFH4ZJ601109', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '1', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1021', 'ZZQFH4ZJ601517', '3', 'Monitor Samsung 22\"', 'S22F350FHL', '1', '', '1', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1022', '029SH9XJ205865E', '3', 'Monitor Samsung 19\"', 'LS19D300HYCZB', '2', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1023', '029SH9XJ202744L', '3', 'Monitor Samsung 19\"', 'LS19D300HYCZB', '2', '', '0', '1', 'APR', '', '1', '');
INSERT INTO `productos` VALUES ('1024', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '0', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1025', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '0', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1026', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '0', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1027', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '0', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1028', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '0', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1029', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '0', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1030', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '0', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1031', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1032', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1033', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1034', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1035', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1036', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1037', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1038', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1039', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1040', '', '2', 'Teclado Bangó USB', 'Bangó Plus', '3', '', '1', '1', 'Donacion de YPF', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1041', '', '1', 'Mouse Reciclado USB', '', '4', '', '0', '1', '', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1042', '', '1', 'Mouse Reciclado USB', '', '4', '', '0', '1', '', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1043', '', '1', 'Mouse Reciclado USB', '', '4', '', '0', '1', '', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1044', '', '1', 'Mouse Reciclado USB', '', '4', '', '0', '1', '', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1045', '', '1', 'Mouse Reciclado USB', '', '4', '', '0', '1', '', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1046', '', '1', 'Mouse Reciclado PS2', '', '5', '', '0', '1', '', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1047', '', '1', 'Mouse Reciclado PS2', '', '5', '', '0', '1', '', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1048', '', '1', 'Mouse Reciclado PS2', '', '5', '', '0', '1', '', 'Fichero Metálico, 2do cajón', '1', '');
INSERT INTO `productos` VALUES ('1049', 'UD1712126366', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '0', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1050', 'UD1712126367', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '1', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1051', 'UD1712126361', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '1', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1052', 'UD1712126271', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '1', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1053', 'UD1712126549', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '1', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1054', 'UD1712126275', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '1', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1055', 'UD1712126274', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '1', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1056', 'UD1712126273', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '1', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1057', 'UD1712126264', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '1', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1058', 'UD1712126265', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '0', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1059', 'UD1712126362', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '1', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1060', 'UD1712126363', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '0', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1061', 'UD1712126364', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '0', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1062', 'UD1712126378', '2', 'Teclado Genius USB', 'KB-125 Value Desktop Keyboard', '6', '', '0', '1', 'APR', 'Fichero Metálico, último cajón', '1', '');
INSERT INTO `productos` VALUES ('1063', '24UML3BGB0A5A0D4', '16', 'Teléfono GrandStream', 'GXP1610/1615', '7', '', '1', '1', '', 'Armario, Estante 1', '1', '');
INSERT INTO `productos` VALUES ('1064', '20EYXQYC9045173E', '16', 'Teléfono GrandStream', 'GXP1400/1405', '7', '', '1', '1', '', 'Armario, Estante 1', '1', '');
INSERT INTO `productos` VALUES ('1065', '20EYZCPC90448ABE', '16', 'Teléfono GrandStream', 'GXP1165', '7', '', '1', '1', '', 'Armario, Estante 1', '1', '');
INSERT INTO `productos` VALUES ('1066', '201600111220', '9', 'Fuente Noga 500w', 'ATX-500 P4', '8', '', '0', '1', '', 'Armario, Estante 2', '1', '');
INSERT INTO `productos` VALUES ('1067', '2174755002362', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1068', '2174755002369', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1069', '2174755005630', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1070', '2174755002363', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1071', '2174755002342', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1072', '2174755001641', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1073', '2174755002350', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1074', '2174755002379', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1075', '2174755002361', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1076', '2174755001646', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1077', '2174755002347', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1078', '2174755002351', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1079', '2174755002354', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '1', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1080', '2174755002366', '4', 'Adaptador USB Inalámbrico 150Mbps', 'TL-WN722N', '9', '', '0', '1', '', 'Armario, Estante 3', '1', '');
INSERT INTO `productos` VALUES ('1081', '8XK62P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '1', '1', 'Donacion de YPF', '', '1', 'Es la 126 en la numeración del Depósito del Juzgado. N° interno de YPF: 441826. Estaba en el Call para prueba. Se retiró y vuelve a estar disponible.');
INSERT INTO `productos` VALUES ('1082', 'DT7Q5P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '1', '1', 'Donacion de YPF', '', '1', 'Es la 127 en la numeración del Depósito del Juzgado. N° interno de YPF: 442033');
INSERT INTO `productos` VALUES ('1083', 'BWK62P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '0', '1', 'Donacion de YPF', '', '1', 'Es la 132 en la numeración del Depósito del Juzgado. N° interno de YPF: 441738');
INSERT INTO `productos` VALUES ('1084', '70HTBQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '1', '1', 'Donacion de YPF', '', '1', 'Es la 134 en la numeración del Depósito del Juzgado. N° interno de YPF: 463749');
INSERT INTO `productos` VALUES ('1085', 'B8HGYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '1', '1', 'Donacion de YPF', '', '1', 'Es la 139 en la numeración del Depósito del Juzgado. N° interno de YPF: 467546');
INSERT INTO `productos` VALUES ('1086', 'FSK62P1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '0', '1', 'Donacion de YPF', '', '1', 'Es la 141 en la numeración del Depósito del Juzgado. N° interno de YPF: 441736');
INSERT INTO `productos` VALUES ('1087', 'FKXJKQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '1', '1', 'Donacion de YPF', '', '1', 'Es la 143 en la numeración del Depósito del Juzgado. N° interno de YPF: 442158');
INSERT INTO `productos` VALUES ('1088', 'F2XJKQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '1', '1', 'Donacion de YPF', '', '1', 'Es la 144 en la numeración del Depósito del Juzgado. N° interno de YPF: 442134');
INSERT INTO `productos` VALUES ('1089', 'B7MFYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '1', '1', 'Donacion de YPF', '', '1', 'Es la 145 en la numeración del Depósito del Juzgado. N° interno de YPF: 467284');
INSERT INTO `productos` VALUES ('1090', 'B5NGYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '1', '1', 'Donacion de YPF', '', '1', 'Es la 146 en la numeración del Depósito del Juzgado. N° interno de YPF: 467287');
INSERT INTO `productos` VALUES ('1091', 'CZGP5M1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '0', '1', 'Donacion de YPF', '', '1', 'Es la 149 en la numeración del Depósito del Juzgado. N° interno de YPF: 460888');
INSERT INTO `productos` VALUES ('1092', 'BBJDYQ1', '5', 'CPU completo marca Dell. Procesador Core 2duo E8400 3GHz. 1 memoria DDR3 2GB c/u. Disco Sata 250G. Lectograb DVD.', 'Optiplex 780', '10', '', '1', '1', 'Donacion de YPF', '', '1', 'Es la 150 en la numeración del Depósito del Juzgado. N° interno de YPF: 467571 ');
INSERT INTO `productos` VALUES ('1093', null, '17', 'Paquete de dos monitores', '', '158', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1094', null, '17', 'Paquete de dos monitores', '', '159', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1095', null, '17', 'Paquete de dos monitores', '', '160', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1096', null, '17', 'Paquete de dos monitores', '', '161', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1097', null, '17', 'Paquete de dos monitores', '', '162', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1098', null, '17', 'PC Completa #1', '', '163', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1099', null, '17', 'Pack teclado y monitor', '', '164', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1100', null, '17', 'Paquete prueba 3', '', '165', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1101', null, '17', '', '', '166', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1102', null, '17', 'Paquete prueba 4', '', '167', '', '0', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1103', null, '17', 'Paquete prueba 5', '', '168', '', '0', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1104', 'asasdas', '1', 'asdddd', 'asdasd', '169', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1105', 'asasdas', '1', 'asdddd', 'asdasd', '170', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1106', 'asdasdasd', '5', 'asdasd', 'asdasd', '171', '', '1', '1', '', '', '1', '');
INSERT INTO `productos` VALUES ('1107', 'asdasd', '1', 'asdasd', '', '172', '', '1', '1', '', '', '1', '');

-- ----------------------------
-- Table structure for tipos_operaciones
-- ----------------------------
DROP TABLE IF EXISTS `tipos_operaciones`;
CREATE TABLE `tipos_operaciones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipos_operaciones
-- ----------------------------
INSERT INTO `tipos_operaciones` VALUES ('1', 'agregar');
INSERT INTO `tipos_operaciones` VALUES ('2', 'retirar');

-- ----------------------------
-- Table structure for tipos_productos
-- ----------------------------
DROP TABLE IF EXISTS `tipos_productos`;
CREATE TABLE `tipos_productos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of tipos_productos
-- ----------------------------
INSERT INTO `tipos_productos` VALUES ('1', 'Mouse');
INSERT INTO `tipos_productos` VALUES ('2', 'Teclado');
INSERT INTO `tipos_productos` VALUES ('3', 'Monitor');
INSERT INTO `tipos_productos` VALUES ('4', 'Router');
INSERT INTO `tipos_productos` VALUES ('5', 'CPU');
INSERT INTO `tipos_productos` VALUES ('6', 'Memoria');
INSERT INTO `tipos_productos` VALUES ('7', 'Gabinete');
INSERT INTO `tipos_productos` VALUES ('8', 'Switch');
INSERT INTO `tipos_productos` VALUES ('9', 'Fuente');
INSERT INTO `tipos_productos` VALUES ('10', 'Mother');
INSERT INTO `tipos_productos` VALUES ('11', 'Lectora');
INSERT INTO `tipos_productos` VALUES ('12', 'Bridge');
INSERT INTO `tipos_productos` VALUES ('13', 'Impresora');
INSERT INTO `tipos_productos` VALUES ('14', 'Sonido');
INSERT INTO `tipos_productos` VALUES ('15', 'Varios');
INSERT INTO `tipos_productos` VALUES ('16', 'Teléfono');
INSERT INTO `tipos_productos` VALUES ('17', 'Paquete');

-- ----------------------------
-- Table structure for toners
-- ----------------------------
DROP TABLE IF EXISTS `toners`;
CREATE TABLE `toners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `cant_deposito` int(11) NOT NULL,
  `cant_oficina` int(11) NOT NULL,
  `activo` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of toners
-- ----------------------------
INSERT INTO `toners` VALUES ('1', 'Lexmark T654', '11', '0', '0');
INSERT INTO `toners` VALUES ('2', 'Lexmark E260', '123', '5', '0');
INSERT INTO `toners` VALUES ('3', 'Lexmark E460', '1', '4', '1');
INSERT INTO `toners` VALUES ('4', 'Lexmark M5610 (504U)', '12', '0', '1');
INSERT INTO `toners` VALUES ('5', 'Lexmark M5310 (504H)', '15', '0', '1');
INSERT INTO `toners` VALUES ('6', 'Lexmark M5315', '3', '0', '1');
INSERT INTO `toners` VALUES ('7', 'Lexmark Unidad de Imagen E260/E460', '6', '10', '1');
INSERT INTO `toners` VALUES ('8', 'Lexmark Unidad de Imagen 500Z', '13', '0', '1');
INSERT INTO `toners` VALUES ('9', 'HP Universal (285A - 36A - 35A)', '3', '0', '1');
INSERT INTO `toners` VALUES ('10', 'HP 12A', '2', '0', '0');
INSERT INTO `toners` VALUES ('11', 'Samsung 5111', '1', '0', '1');
INSERT INTO `toners` VALUES ('12', 'asd', '9', '0', '0');
INSERT INTO `toners` VALUES ('13', 'asd777', '7', '0', '0');
INSERT INTO `toners` VALUES ('14', 'Hola', '50', '0', '0');
INSERT INTO `toners` VALUES ('15', 'Asreje', '15', '0', '1');
INSERT INTO `toners` VALUES ('16', 'asd', '0', '0', '0');

-- ----------------------------
-- Table structure for usuarios
-- ----------------------------
DROP TABLE IF EXISTS `usuarios`;
CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(4096) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(25) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `roles` json NOT NULL COMMENT '(DC2Type:json_array)',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of usuarios
-- ----------------------------
INSERT INTO `usuarios` VALUES ('4', 'asd@hotmail.com', 'admin', '$2y$13$Mkw4gGhGlJtU8Vzt.IOCnuocknHLdaUH/fow0PLFt3MTlGMHPOIKu', 'admin', 'dmin');
INSERT INTO `usuarios` VALUES ('5', 'asd@hotmail.com', 'aasd', '$2y$13$NKrZ4PY1gCGV47uvv5zq6uGdfbSqkqtQXmultqIIgvS0GcUcZd3b6', 'asd', 'asd');

-- ----------------------------
-- Table structure for usuarios2
-- ----------------------------
DROP TABLE IF EXISTS `usuarios2`;
CREATE TABLE `usuarios2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `apellido` varchar(255) NOT NULL,
  `jwt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of usuarios2
-- ----------------------------
INSERT INTO `usuarios2` VALUES ('4', 'asdasd@gmail.com', 'asdasd', '$2b$08$OkRQDIzuxWUQzVEIOH7FKObZZY19LziMIGnGakzCzKUg8sIxbOQvG', 'Julian', 'Almandos', 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiaWF0IjoxNTU0OTE5NTcxLCJleHAiOjE1NTQ5MjAxNzF9.Xc4VZtosPWKIW-Rl47sJPHHdgqWaDxx8E-cz7Bfajw0');
INSERT INTO `usuarios2` VALUES ('5', 'asereje@gmail.com', 'asereje', '$2b$08$rUgXBZ85d4gtp6vn5fBHwuQDzYKMu6wEhUZBQldO5lgAgjjF/3HEK', 'Matias', 'Suarez', null);
