import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    frutas: [
      {nombre: 'Manzana', cantidad:0},
      {nombre: 'Pera', cantidad: 0},
      {nombre: 'Naranja', cantidad: 0}
    ]
  },
  mutations: {
    sumarFruta(state,index){
      state.frutas[index].cantidad++
    },
    restarFruta(state,index){
      state.frutas[index].cantidad--
    },
    reiniciarFrutas(state){
      state.frutas.forEach(e => {e.cantidad=0})
    }
  },
  actions: {

  }
})
