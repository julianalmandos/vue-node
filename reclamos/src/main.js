import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faUser, faGlobe, faTable, faCog, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import * as VueGoogleMaps from 'vue2-google-maps'
//import firebase from 'firebase'

library.add(faGlobe,faUser,faTable,faCog,faSignOutAlt)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAO1QxdMtOQW1nMGLrFn1o-CrMLitzmkuM',
    libraries: 'places',
  }
})

/*var config = {
  apiKey: "AIzaSyD7U_EI5OrFbUdViK_h9FFow-shQ_rnRnU",
  authDomain: "reclamos-50350.firebaseapp.com",
  databaseURL: "https://reclamos-50350.firebaseio.com",
  projectId: "reclamos-50350",
  storageBucket: "reclamos-50350.appspot.com",
  messagingSenderId: "614368932657"
};
firebase.initializeApp(config);*/ 

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
