import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    reclamos: [],
  },
  mutations: {
    setReclamos(state, reclamos) {
      state.reclamos=reclamos;
    }
  },
  actions: {
    getReclamos: async function ({ commit }) {
      const data = await fetch('http://localhost:3000');
      const reclamos = await data.json();
      commit('setReclamos',reclamos);
    }
  },
})
