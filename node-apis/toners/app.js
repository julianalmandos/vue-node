var express = require('express');
var mysql = require('mysql');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

var app = express();
var conn = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',  
  password : '',
  database : 'inventario'
});

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', '*');
  next();
});

app.get('/', function (req, res) {
  var sql="SELECT * FROM toners t WHERE t.activo=1";
  conn.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
});

app.post('/agregar', function (req, res) {
  console.log(req.body);  
  var sql="UPDATE toners t SET t.cant_deposito = t.cant_deposito+"+req.body.cantidad+" WHERE t.id="+req.body.id;  
  console.log(sql);
  conn.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
});

app.post('/retirar', function (req, res) {
  console.log(req.body);  
  var sql="UPDATE toners t SET t.cant_deposito = t.cant_deposito-"+req.body.cantidad+" WHERE t.id="+req.body.id;  
  console.log(sql);
  conn.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
});

app.post('/mover', function (req, res) {
  console.log(req.body);  
  var sql="UPDATE toners t SET t.cant_oficina = cant_oficina +"+req.body.cantidad+", t.cant_deposito = t.cant_deposito-"+req.body.cantidad+" WHERE t.id="+req.body.id;  
  console.log(sql);
  conn.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
});

app.post('/nuevo', function (req, res) {
  console.log(req.body);  
  var sql="INSERT INTO toners (nombre,cant_deposito,cant_oficina) VALUES ('"+req.body.nombre+"',0,0)";  
  console.log(sql);
  conn.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
});

app.post('/eliminar', function (req, res) {
  console.log(req.body);  
  var sql="UPDATE toners t SET t.activo=0 WHERE t.id="+req.body.id;  
  console.log(sql);
  conn.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
});

app.post('/registrar', function(req, res) {
  //console.log(req.body.data);
  var contraseña=bcrypt.hashSync(req.body.data.password,8);
  var sql="INSERT INTO usuarios2 (email,username,password,nombre,apellido) VALUES ('"+req.body.data.email+"','"+req.body.data.usuario+"','"+contraseña+"','"+req.body.data.nombre+"','"+req.body.data.apellido+"')";
  console.log(sql);
  conn.query(sql, function (err, result) {
    if (err) throw err;
    res.send(result);
  });
});

app.post('/login', (req, res) => {
  console.log(req.body.data);
  var sql="SELECT * FROM usuarios2 us WHERE us.email='"+req.body.data.email+"'";
  conn.query(sql, function (err, result) {
    console.log(result);
    if (err) throw err;
    if (result[0]==null){
      console.log('entra'); 
      return res.status(401).send('Ese e-mail no se encuentra registrado.');
    }else{
      let passwordIsValid = bcrypt.compareSync(req.body.data.password, result[0].password);
      if (!passwordIsValid) return res.status(401).send({ auth: false, token: null, msg: 'Contraseña incorrecta.'});
      let token = jwt.sign({ id: result[0].id }, 'shhhhh', { expiresIn: 600 // expires in 24 hours
      });
      console.log(token);
      res.status(200).send({ auth: true, token: token, user: result[0] });
    }
  });
})

app.post('/validartoken', (req, res) => {
  try {
    var decoded = jwt.verify(req.body.token, 'shhhhh');
    res.send(true);
  } catch(err) {
    res.send(false);
  }
})

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

