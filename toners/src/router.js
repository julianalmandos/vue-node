import Vue from 'vue'
import Router from 'vue-router'
import axios from 'axios'
import store from '@/store.js'

Vue.use(Router)
var router;
export default router = new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/Home.vue'),
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/historial',
      name: 'historial',
      component: () => import('./views/Historial.vue'),
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/registrar',
      name: 'registrar',
      component: () => import('./views/Registrar.vue'),
    },
    {
      path: '/login',
      name: 'login',
      component: () => import('./views/Login.vue'),
    }
  ]
})

router.beforeEach((to, from, next) => {
  if(localStorage.getItem('jwt')!=null){
    if(to.matched.some(record => record.meta.requiresAuth)){
      console.log('entra');
      var tokenValido=false;
      axios.post('//localhost:3000/validartoken', {
        token: localStorage.getItem('jwt')
      })
      .then(response => {
        console.log('response: '+response.data);
        if(response.data){
          console.log('sigueentra');
          next();
        }else{
          console.log('sigueelse')
          //desloguear usuario y redirigir a login
          store.dispatch('cerrarSesionUsuario');
          next({
            path: '/login',
            params: { nextUrl: to.fullPath }
          });
        }
      });
      console.log('sigue')
      //chequeo si no esta vencido y dependiendo la ruta hago lo que sea
    }else if(to.matched.some(record => record.meta.guest)){
      next()
    }else{
      next({
        path: '/',
      });
    }
  }else {
    if(to.matched.some(record => record.meta.requiresAuth)){
      next({
        path: '/login',
        params: { nextUrl: to.fullPath }
      });
    }else if(to.matched.some(record => record.meta.guest)){
      next();
    }else{
      next();
    }
  }
})

/*router.beforeEach((to, from, next) => {
  if(to.matched.some(record => record.meta.requiresAuth)) {
      if (localStorage.getItem('jwt') == null) {
          next({
              path: '/login',
              params: { nextUrl: to.fullPath }
          })
      } else {
          //let user = JSON.parse(localStorage.getItem('user'))
          next();
      }
  } else if(to.matched.some(record => record.meta.guest)) {
      if(localStorage.getItem('jwt') == null){
          next()
      }
      else{
          next({ name: 'userboard'})
      }
  }else {
      next() 
  }
})*/

