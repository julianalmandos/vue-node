import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import createPersistedState from 'vuex-persistedstate'
import router from '@/router.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    toners: [],
    usuario: null,
  },
  plugins: [createPersistedState({
    paths: ['usuario'],
  })],
  mutations: {
    setToners(state,toners){
      state.toners=toners;
    },
    iniciarSesionUsuario(state,data){
      state.usuario=data;
    },
    cerrarSesionUsuario(state) {
      state.usuario=null;
    }
  },
  actions: {
    getToners({commit}) {
      axios.get('//localhost:3000')
        .then(function(response){
          commit('setToners',response.data);
        })
        .catch(function(error){
          console.log(error);
        })
    },
    agregarToners({dispatch},data) {
      axios.post('//localhost:3000/agregar', {
          id: data.idActual,
          cantidad: data.cantidad
        })
        .then(function(response){
          console.log('entra');
          dispatch('getToners');
        })
        .catch(function(error){
          console.log('error: '+error);
        });
    },
    retirarToners({dispatch},data) {
      axios.post('//localhost:3000/retirar', {
          id: data.idActual,
          cantidad: data.cantidad
        })
        .then(function(response){
          console.log('entra');
          dispatch('getToners');
        })
        .catch(function(error){
          console.log('error: '+error);
        });
    },
    moverToners({dispatch},data) {
      axios.post('//localhost:3000/mover', {
          id: data.idActual,
          cantidad: data.cantidad
        })
        .then(function(response){
          console.log('entra');
          dispatch('getToners');
        })
        .catch(function(error){
          console.log('error: '+error);
        });
    },
    nuevoToner({dispatch},data) {
      axios.post('//localhost:3000/nuevo', {
          nombre: data,
        })
        .then(function(response){
          //console.log('entra');
          dispatch('getToners');
        })
        .catch(function(error){
          console.log('error: '+error);
        });
    },
    eliminarToner({dispatch},data){
      axios.post('//localhost:3000/eliminar', {
          id: data,
        })
        .then(function(response){
          console.log('entra');
          dispatch('getToners');
        })
        .catch(function(error){
          console.log('error: '+error);
        });
    },
    iniciarSesionUsuario({commit},data){
      commit('iniciarSesionUsuario',data);
    },
    cerrarSesionUsuario({commit}){
      localStorage.removeItem('user');
      localStorage.removeItem('jwt');
      commit('cerrarSesionUsuario');
      router.push('login');      
    }
  }
})
